<?php

$host="localhost";
$username="root";
$passward="";
$dbname="survey";

$conn=mysqli_connect($host,$username,$passward,$dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

    $surveyDisable = surveyDisable($conn);
    echo $surveyDisable;


function surveyDisable($conn){
    try {
        // disable survey
        $query = "UPDATE survey_header SET status = 0 WHERE survey_header.end_date < CURRENT_DATE()";
        $result = mysqli_query($conn, $query);
        $data = [
            'status' => 200,
            'message' => 'Survey disabled',
        ];
        return json_encode($data);
    } catch (Exception $e) {
        return error422($e->getMessage());
    }
}


?>
