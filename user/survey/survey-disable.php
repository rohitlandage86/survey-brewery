<?php
// error_reporting(0);
require '../../connection/conn.php';
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$requestMethod = $_SERVER["REQUEST_METHOD"];
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    http_response_code(200);
    exit;
}

// Create a MySQLi connection
$host="localhost";
$username="root";
$passward="";
$dbname="survey";

$conn=mysqli_connect($host,$username,$passward,$dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if ($requestMethod == "GET") {
    $surveyDisable = surveyDisable($conn);
    echo $surveyDisable;
} else {
    $data = [
        'status' => 405,
        'message' => $requestMethod . ' Method Not Allowed'
    ];
    header("HTTP/1.0 405 Method Not Allowed");
    echo json_encode($data);
}

function surveyDisable($conn){
    try {
        // disable survey
        $query = "UPDATE survey_header SET status = 0 WHERE survey_header.end_date < CURRENT_DATE()";
        $result = mysqli_query($conn, $query);
        $data = [
            'status' => 200,
            'message' => 'Survey disabled',
        ];
        return json_encode($data);
    } catch (Exception $e) {
        return error422($e->getMessage());
    }
}


?>
