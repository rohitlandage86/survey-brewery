<?php
require '../../connection/conn.php';
function error422($message)
{
    $data = [
        "status" => 422,
        "message" => $message
    ];
    header("HTTP/1.0 422 unprocessable Entity");
    echo json_encode($data);
    exit();
}

function createSurveyHeader($inputData)
{
    global $conn;
    $survey_title = mysqli_real_escape_string($conn, $inputData['title']);
    $end_date = mysqli_real_escape_string($conn, $inputData['endDate']);
    $level_id = mysqli_real_escape_string($conn, $inputData['level']);
    $gender = mysqli_real_escape_string($conn, $inputData['gender']);
    $region_id = mysqli_real_escape_string($conn, $inputData['region']);
    $profession_id = mysqli_real_escape_string($conn, $inputData['profession']);
    $sub_profession_id = mysqli_real_escape_string($conn, $inputData['sub_profession']);
    $possibleResponseCount = mysqli_real_escape_string($conn, $inputData['possibleResponseCount']);
    $user_id = mysqli_real_escape_string($conn, $inputData['user_id']);
    $category = ""; // Assuming you have access to the table containing the 'category' information

    mysqli_begin_transaction($conn);

    try {
        // Retrieve the category for the user_id from the related table
        $categoryQuery = "SELECT category FROM untitled WHERE user_id = '$user_id'";
        $categoryResult = mysqli_query($conn, $categoryQuery);
        if ($categoryResult && mysqli_num_rows($categoryResult) > 0) {
            $row = mysqli_fetch_assoc($categoryResult);
            $category = $row['category'];
        }

        $questions = $inputData['questions'];
        if (empty(trim($survey_title))) {
            return error422('Enter survey title');
        } elseif (empty(trim($user_id))) {
            return error422('user id is required');
        } elseif (empty(trim($end_date))) {
            return error422('End date is required');
        } elseif (empty(trim($level_id))) {
            return error422('Level is required');
        } elseif (empty(trim($gender)) && trim($gender) != 0) {
            return error422('Gender is required');
        } elseif (empty(trim($possibleResponseCount)) && trim($possibleResponseCount) != 0) {
            return error422('Possiblle response count is required');
        } elseif (empty(trim($region_id)) && trim($region_id) != 0) {
            return error422('Region is required');
        } elseif (empty(trim($profession_id)) && trim($profession_id) != 0) {
            return error422('Profession is required');
        } else {
            // Check if the survey title already exists for the user
            $query = "SELECT * FROM survey_header WHERE user_id='$user_id' AND survey_title='$survey_title' AND del_flag=1";
            $result = mysqli_query($conn, $query);
            if (mysqli_num_rows($result) > 0) {
                return error422('This Survey title already exists');
            } else {
                $start_date = date('Y-m-d');
                if (empty($sub_profession_id)) {
                    $sub_profession_id = 0;
                }
                $query = "INSERT INTO survey_header (survey_title, start_date, end_date, level_id, gender, region_id, profession_id, sub_profession_id,possible_res_count, user_id) VALUES ('$survey_title', '$start_date', '$end_date', $level_id, $gender, $region_id, $profession_id, $sub_profession_id,$possibleResponseCount, $user_id)";
                $result = mysqli_query($conn, $query);
                $surveyId = mysqli_insert_id($conn);

                // Check if the survey header insertion was successful
                if ($result) {
                    // Insert questions
                    foreach ($questions as $question) {
                        $questionText = mysqli_real_escape_string($conn, $question['question']);
                        $questionNo = mysqli_real_escape_string($conn, $question['question_no']);
                        $questionTypeId = isset($question['input_type']['question_type_id']) ? mysqli_real_escape_string($conn, $question['input_type']['question_type_id']) : null;

                        $query = "INSERT INTO survey_question (survey_id, question, question_no, question_type_id) VALUES ($surveyId, '$questionText', $questionNo, ";

                        if ($questionTypeId !== null) {
                            $query .= "'$questionTypeId')";
                        } else {
                            $query .= "NULL)";
                        }

                        $result = mysqli_query($conn, $query);

                        // Check if the question insertion was successful
                        if ($result) {
                            // Get the inserted question ID
                            $questionId = mysqli_insert_id($conn);

                            // Insert options for each question
                            if (isset($question['options']) && is_array($question['options'])) {
                                foreach ($question['options'] as $option) {
                                    if (!empty($option['option'])) {
                                        $optionText = mysqli_real_escape_string($conn, $option['option']);
                                        $optionQuery = "INSERT INTO survey_question_option (survey_question_id, `option`) VALUES ($questionId, '$optionText')";
                                        $optionResult = mysqli_query($conn, $optionQuery);

                                        // Handle option insertion result if needed
                                        if (!$optionResult) {
                                            throw new Exception('Failed to insert survey_question_option record');
                                        }
                                    }
                                }
                            }
                        } else {
                            throw new Exception('Failed to insert survey_question record');
                        }
                    }



                    $points = 0; // Define the $points variable before the condition

                    if ($category != "1") {
                        $queryLevel = "SELECT points_after_create FROM level WHERE level_id='$level_id' AND del_flag='1'";
                        $result = mysqli_query($conn, $queryLevel);

                        if ($result && mysqli_num_rows($result) > 0) {
                            $row = mysqli_fetch_assoc($result);
                            $points = $row['points_after_create'];
                            $insertkarmaLog = "INSERT INTO karma_points_log (user_id, level_id, survey_id, points, survey_type) VALUES ($user_id, $level_id, $surveyId, '$points', 'create')";
                            $result = mysqli_query($conn, $insertkarmaLog);
                            $kpl_id = mysqli_insert_id($conn); // Retrieve kpl id from karma points log table
                            if (!$result) {
                                throw new Exception('Failed to insert karma_points_log record');
                            }
                        }
                        $query = "SELECT user_id FROM karma_point WHERE user_id = '$user_id'";
                        $result = mysqli_query($conn, $query);

                        if ($result && mysqli_num_rows($result) > 0) {
                            $query = "SELECT total FROM karma_point WHERE user_id = $user_id";
                            $query_run = mysqli_query($conn, $query);
                            $total = mysqli_fetch_assoc($query_run);
                            $tot = $total["total"];
                            $closing = $tot - (int)$points;
                            $query = "UPDATE karma_point SET opening = $tot, points = '$points', closing = '$closing', total = '$closing' WHERE user_id = $user_id";
                            $result = mysqli_query($conn, $query);
                            $query = "UPDATE karma_points_log SET  total = '$closing' WHERE kp_id = $kpl_id";
                            $result1 = mysqli_query($conn, $query);
                            if (!$result) {
                                throw new Exception('Failed to update karma_point record');
                            }
                        } else {
                            throw new Exception('Please fill survey for karma_point');
                        }
                    }

                    mysqli_commit($conn);

                    $data = [
                        'status' => 201,
                        'message' => 'The survey has been created successfully',
                    ];
                    return json_encode($data);
                } else {
                    throw new Exception('Failed to insert survey_header record');
                }
            }
        }
    } catch (Exception $e) {
        mysqli_rollback($conn);
        $data = [
            'status' => 500,
            'message' => $e->getMessage(),
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }
}


function getAllSurveyHeaderList($user_id, $minToMax, $maxToMin, $level_id)
{
    global $conn;

    // Fetch the user's gender from the survey_header table
    $user_gender_query = "SELECT gender ,region_id,profession_id,sub_profession_id FROM user_registration WHERE user_id = $user_id LIMIT 1";
    $user_gender_result = mysqli_query($conn, $user_gender_query);
    $user_gender_row = mysqli_fetch_assoc($user_gender_result);
    $user_gender = $user_gender_row['gender'];
    $user_region_id = $user_gender_row['region_id'];
    $user_profession_id = $user_gender_row['profession_id'];
    $user_sub_profession_id = $user_gender_row['sub_profession_id'];
    
    $query = "SELECT survey_header.user_id, survey_header.end_date, survey_header.start_date, survey_header.survey_id, survey_header.status, 
          (SELECT COUNT(*) FROM survey_question WHERE survey_question.survey_id = survey_header.survey_id) AS questionsCount, 
          survey_header.survey_title, survey_header.level_id 
      FROM survey_header 
      WHERE NOT EXISTS (
          SELECT survey_id 
          FROM fill_survey_header 
          WHERE fill_survey_header.survey_id = survey_header.survey_id 
          AND fill_survey_header.user_id = $user_id
      ) 
      AND survey_header.user_id <> $user_id 
      AND survey_header.del_flag = '1' 
      AND survey_header.status = '1' 
      AND end_date >= CURRENT_DATE()  
      AND survey_header.level_id = '$level_id' 
      AND (survey_header.gender = '0' OR survey_header.gender = '$user_gender' )
      AND (survey_header.region_id = '0' OR survey_header.region_id = '$user_region_id')
      AND (survey_header.profession_id = '0' OR survey_header.profession_id = '$user_profession_id')
      AND (survey_header.sub_profession_id = '0' OR survey_header.sub_profession_id = '$user_sub_profession_id')
      ";
  // Add ORDER BY clause based on the provided flags
    if ($minToMax == 'true') {
        $query .= " ORDER BY questionsCount ASC";
    } elseif ($maxToMin == 'true') {
        $query .= " ORDER BY questionsCount DESC";
    } else {
        $query .= " ORDER BY survey_id DESC"; // Use any column name here
    }

    $query_run = mysqli_query($conn, $query);
    if ($query_run) {
        $res = mysqli_fetch_all($query_run, MYSQLI_ASSOC);

        // If there are no matching surveys after filtering, return 404
        if (empty($res)) {
            $data = [
                'status' => 404,
                'data' => [],
                'message' => 'No survey found'
            ];
            header("HTTP/1.0 404 No survey found");
            return json_encode($data);
        }

        $data = [
            'status' => 201,
            'message' => 'survey list Fetched Successfully',
            'data' => $res
        ];
        return json_encode($data);
    } else {
        $data = [
            'status' => 500,
            'message' => 'Internal Server Error',
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }
}


//for user dashboard...
function getAllBasicDetailsOfSurveyList($page, $perPage, $user_id)
{
    global $conn;
    $query = "SELECT survey_header.user_id, survey_header.end_date, survey_header.start_date, survey_header.survey_id, survey_header.status, COUNT(survey_question.survey_question_id) AS questionsCount, survey_header.survey_title, level.level, survey_header.cts FROM survey_question JOIN survey_header ON survey_header.survey_id = survey_question.survey_id JOIN level ON survey_header.level_id = level.level_id WHERE survey_header.user_id = $user_id AND survey_header.del_flag = '1' AND survey_header.end_date >= CURRENT_DATE() GROUP BY survey_header.survey_id, survey_header.user_id, survey_header.end_date, survey_header.start_date, survey_header.status, survey_header.survey_title, level.level";
    $query .= " ORDER BY survey_header.start_date DESC";
    // Get the total count of surveys
    $countQuery = "SELECT COUNT(*) AS total FROM ($query) AS count";
    $countResult = mysqli_query($conn, $countQuery);
    $totalCount = 0;
    if ($countResult) {
        $countRow = mysqli_fetch_assoc($countResult);
        $totalCount = $countRow['total'];
    }
    // Add pagination to the main query
    if ($page !== null && $perPage !== null) {
        $start = ($page - 1) * $perPage;
        $query .= " LIMIT $start, $perPage";
    }
    $query_run = mysqli_query($conn, $query);

    if ($query_run) {
        if (mysqli_num_rows($query_run) > 0) {
            $res = mysqli_fetch_all($query_run, MYSQLI_ASSOC);
            $data = [
                'status' => 201,
                'message' => 'Survey list fetched successfully',
                'data' => $res
            ];
            // Add pagination information to the response
            if ($page !== null && $perPage !== null) {
                $data['pagination'] = [
                    'per_page' => $perPage,
                    'total' => $totalCount,
                    'current_page' => $page,
                    'last_page' => ceil($totalCount / $perPage)
                ];
            }
            return json_encode($data);
        } else {
            $data = [
                'status' => 404,
                'data' => [],
                'message' => 'No survey found'
            ];
            header("HTTP/1.0 404 No survey found");
            return json_encode($data);
        }
    } else {
        $data = [
            'status' => 500,
            'message' => 'Internal Server Error',
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }
}




function getSurveyById($id, $user_id)
{
    global $conn;
    $query = "SELECT s.*, l.level, l.points_after_fill, l.points_after_create, r.region, p.profession, sp.sub_profession, q.survey_question_id, q.question, q.question_no, q.question_type_id, qt.input_type, o.survey_question_option_id, o.option
    FROM survey_header s 
    JOIN level l ON s.level_id = l.level_id 
    LEFT JOIN regions r ON s.region_id = r.region_id 
    LEFT JOIN profession p ON s.profession_id = p.profession_id 
    LEFT JOIN sub_profession sp ON s.sub_profession_id = sp.sub_profession_id 
    LEFT JOIN survey_question q ON s.survey_id = q.survey_id 
    LEFT JOIN question_type qt ON q.question_type_id = qt.question_type_id 
    LEFT JOIN survey_question_option o ON q.survey_question_id = o.survey_question_id 
    WHERE s.user_id != $user_id AND s.del_flag = 1 ";

    if ($id !== null) {
        $query .= " AND s.survey_id = $id ORDER BY q.survey_question_id ASC";
    }
    $query_run = mysqli_query($conn, $query);
    if ($query_run) {
        if (mysqli_num_rows($query_run) > 0) {
            $data = array();
            while ($row = mysqli_fetch_assoc($query_run)) {
                $survey_id = $row['survey_id'];
                if (!isset($data[$survey_id])) {
                    $data[$survey_id] = array(
                        'title' => $row['survey_title'],
                        'startDate' => $row['start_date'],
                        'endDate' => $row['end_date'],
                        'possible_res_count' => $row['possible_res_count'],
                        'level' => array(
                            'levelId' => $row['level_id'],
                            'level' => $row['level'],
                            'points_after_fill' => $row['points_after_fill'],
                            'points_after_create' => $row['points_after_create'],
                        ),
                        'region' => array(
                            'regionId' => $row['region_id'],
                            'region' => $row['region']
                        ),
                        'profession' => array(
                            'professionId' => $row['profession_id'],
                            'profession' => $row['profession']
                        ),
                        'subProfession' => array(
                            'subProfessionId' => $row['sub_profession_id'],
                            'subProfession' => $row['sub_profession']
                        ),
                        'questions' => array()
                    );
                }

                $question_id = $row['survey_question_id'];
                $question = $row['question'];
                $question_no = $row['question_no'];
                $question_type_id = $row['question_type_id'];
                $input_type = $row['input_type'];
                $option_id = $row['survey_question_option_id'];
                $option = $row['option'];

                // Check if the question exists in the data array
                if (!isset($data[$survey_id]['questions'][$question_id])) {
                    $data[$survey_id]['questions'][$question_id] = array(
                        'question' => $question,
                        'question_no' => $question_no,
                        'question_type_id' => $question_type_id,
                        'input_type' => $input_type,
                        'options' => array()
                    );
                }

                // Add the option to the question
                if ($option_id && $option) {
                    $data[$survey_id]['questions'][$question_id]['options'][] = array(
                        'optionId' => $option_id,
                        'option' => $option
                    );
                }
            }

            $survey_array = array();
            foreach ($data as $survey_id => $survey_data) {
                $questions = array();
                foreach ($survey_data['questions'] as $question_id => $question_data) {
                    $questions[] = array(
                        'questionId' => $question_id,
                        'question_type_id' => $question_data['question_type_id'],
                        'input_type' => $question_data['input_type'],
                        'question' => $question_data['question'],
                        'question_no' => $question_data['question_no'],
                        'options' => $question_data['options']
                    );
                }

                $survey_array[] = array(
                    'surveyId' => $survey_id,
                    'title' => $survey_data['title'],
                    'startDate' => $survey_data['startDate'],
                    'endDate' => $survey_data['endDate'],
                    'possible_res_count' => $survey_data['possible_res_count'],
                    'level' => $survey_data['level'],
                    'region' => $survey_data['region'],
                    'profession' => $survey_data['profession'],
                    'subProfession' => $survey_data['subProfession'],
                    'questions' => $questions
                );
            }

            $response = array(
                'status' => 201,
                'message' => 'Survey details fetched successfully',
                'data' => $survey_array
            );

            return json_encode($response);
        } else {
            $response = array(
                'status' => 404,
                'message' => 'No survey found',
                'data' => array()
            );

            header("HTTP/1.0 404 Not Found");
            return json_encode($response);
        }
    } else {
        $response = array(
            'status' => 500,
            'message' => 'Internal Server Error'
        );

        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($response);
    }
}



//set survey enable or disable 
function surveyEnableDisable($status, $id)
{
    global $conn;
    if ($status != 1 && $status != 0) {
        return error422('Invalid status');
    }

    // Check if the survey exists in the survey_header table
    $query = "SELECT * FROM survey_header WHERE survey_id = '$id'";
    $result = mysqli_query($conn, $query);
    if (mysqli_num_rows($result) == 0) {
        return error422('Survey not found');
    }

    // Check if the end date is not in the past
    $query = "SELECT * FROM survey_header WHERE survey_id='$id' AND end_date >= CURDATE()";
    $result = mysqli_query($conn, $query);
    if (mysqli_num_rows($result) == 0) {
        return error422("You can't enable the survey as the end date has passed.");
    }

    // Begin transaction
    mysqli_autocommit($conn, false);

    try {
        // Enable or disable survey
        $query = "UPDATE survey_header SET status = $status WHERE survey_id = '$id'";
        $result = mysqli_query($conn, $query);

        // Commit the transaction if all steps succeed
        mysqli_commit($conn);
        mysqli_autocommit($conn, true);

        // Return the survey ID and success message
        $message = ($status == 1) ? 'Activated' : 'Deactivated';
        $data = [
            'status' => 200,
            'message' => 'Survey ' . $message,
        ];
        return json_encode($data);
    } catch (Exception $e) {
        // Rollback the transaction if any step fails
        mysqli_rollback($conn);
        mysqli_autocommit($conn, true);

        // Handle the exception
        return error422($e->getMessage());
    }
}

function surveyDisable()
{
    global $conn;

    // Begin transaction
    mysqli_autocommit($conn, false);

    try {
        // disable survey
        $query = "UPDATE survey_header SET status = 0 WHERE survey_header.end_date < CURRENT_DATE()";

        $result = mysqli_query($conn, $query);
        // Commit the transaction if all steps succeed
        mysqli_commit($conn);
        mysqli_autocommit($conn, true);

        $data = [
            'status' => 200,
            'message' => 'Survey disable',
        ];
        return json_encode($data);
    } catch (Exception $e) {
        // Rollback the transaction if any step fails
        mysqli_rollback($conn);
        mysqli_autocommit($conn, true);

        // Handle the exception
        return error422($e->getMessage());
    }
}

function surveySummary($surveyId)
{
    global $conn;

    try {
        // Retrieve survey details
        $query = "SELECT sh.*, l.level, l.points_after_fill, l.points_after_create
                  FROM survey_header sh
                  LEFT JOIN level l ON sh.level_id = l.level_id
                  WHERE sh.survey_id = '$surveyId' LIMIT 1";
        $result = mysqli_query($conn, $query);

        if (!$result) {
            throw new Exception('Failed to retrieve survey details');
        }
        $surveyDetails = mysqli_fetch_assoc($result);
        $data = [
            'status' => 201,
            'message' => 'Survey summary fetched successfully',
            'data' => [
                [
                    'surveyId' => $surveyDetails['survey_id'],
                    'title' => $surveyDetails['survey_title'],
                    'startDate' => $surveyDetails['start_date'],
                    'endDate' => $surveyDetails['end_date'],
                    'possible_res_count' => $surveyDetails['possible_res_count'],
                    'level' => [
                        'levelId' => $surveyDetails['level_id'],
                        'level' => $surveyDetails['level'],
                        'points_after_fill' => $surveyDetails['points_after_fill'],
                        'points_after_create' => $surveyDetails['points_after_create']
                    ],
                    'questions' => [] // We will populate this later
                ]
            ]
        ];
        
        // Retrieve questions and options for the survey
        $query = "SELECT sq.*, qo.survey_question_option_id, qo.option ,qt.input_type
                  FROM survey_question sq
                  LEFT JOIN survey_question_option qo ON sq.survey_question_id = qo.survey_question_id
                  LEFT JOIN question_type qt ON sq.question_type_id = qt.question_type_id
                  WHERE sq.survey_id = '$surveyId'";
        $result = mysqli_query($conn, $query);

        if ($result) {
            while ($row = mysqli_fetch_assoc($result)) {
                $questionId = $row['survey_question_id'];
                
                // Check if the question already exists in the data structure
                $questionExists = false;
                foreach ($data['data'][0]['questions'] as &$existingQuestion) {
                    if ($existingQuestion['questionId'] === $questionId) {
                        $questionExists = true;
                        $existingQuestion['options'][] = [
                            'optionId' => $row['survey_question_option_id'],
                            'option' => $row['option'],
                            'count' => null // We will populate this later
                        ];
                        break;
                    }
                }
                
                if (!$questionExists) {
                    $question = [
                        'questionId' => $questionId,
                        'question_type_id' => $row['question_type_id'],
                        'input_type' => $row['input_type'],
                        'question' => $row['question'],
                        'options' => [
                            [
                                'optionId' => $row['survey_question_option_id'],
                                'option' => $row['option'],
                                'count' => null // We will populate this later
                            ]
                        ]
                    ];
                    $data['data'][0]['questions'][] = $question;
                }
            }
        }

        // Populate option counts
        foreach ($data['data'][0]['questions'] as &$question) {
            foreach ($question['options'] as &$option) {
                $optionCountQuery = "SELECT COUNT(*) AS count 
                                     FROM fill_survey_question  
                                     WHERE survey_id = '$surveyId' 
                                     AND survey_question_id = '{$question['questionId']}' 
                                     AND survey_question_option_id = '{$option['optionId']}'";
                $optionCountResult = mysqli_query($conn, $optionCountQuery);

                if ($optionCountResult) {
                    $optionCountRow = mysqli_fetch_assoc($optionCountResult);
                    $option['count'] = $optionCountRow['count'];
                }
            }
        }

        // Finally, encode the data as JSON and return
        return json_encode($data);

    } catch (Exception $e) {
        $data = [
            'status' => 500,
            'message' => 'An error occurred: ' . $e->getMessage(),
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }
}