<?php
require '../../connection/conn.php';
function error422($message)
{
    $data = [
        "status" => 422,
        "message" => $message
    ];
    header("HTTP/1.0 422 unprocessable Entity");
    echo json_encode($data);
    exit();
}

function getAllDetails($user_id)
{
    global $conn;

    // Check if the user ID exists
    $query = "SELECT * FROM untitled WHERE user_id='$user_id' AND del_flag=1";
    $result = mysqli_query($conn, $query);

    if (mysqli_num_rows($result) == 0) {
        return error422('User ID does not exist');
    }

    // Get karma points
    $karmaPoints = 0;
    $query = "SELECT total FROM karma_point WHERE user_id=?";
    $stmt = mysqli_prepare($conn, $query);
    mysqli_stmt_bind_param($stmt, "i", $user_id);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $karmaPoints);
    mysqli_stmt_fetch($stmt);
    mysqli_stmt_close($stmt);

    // Get survey create count live
    $surveyCreateCountLive = 0;
    $query = "SELECT COUNT(*) FROM survey_header WHERE user_id=? AND `status`='1'";
    $stmt = mysqli_prepare($conn, $query);
    mysqli_stmt_bind_param($stmt, "i", $user_id);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $surveyCreateCountLive);
    mysqli_stmt_fetch($stmt);
    mysqli_stmt_close($stmt);

    // Get survey create count close
    $surveyCreateCountClose = 0;
    $query = "SELECT COUNT(*) FROM survey_header WHERE user_id=? AND `status`='0'";
    $stmt = mysqli_prepare($conn, $query);
    mysqli_stmt_bind_param($stmt, "i", $user_id);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $surveyCreateCountClose);
    mysqli_stmt_fetch($stmt);
    mysqli_stmt_close($stmt);
    // Get survey create count close
    $surveyFillCount = 0;
    $query = "SELECT COUNT(*) FROM fill_survey_header WHERE user_id=?";
    $stmt = mysqli_prepare($conn, $query);
    mysqli_stmt_bind_param($stmt, "i", $user_id);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $surveyFillCount);
    mysqli_stmt_fetch($stmt);
    mysqli_stmt_close($stmt);
    // Get level
    $level = '';
    $level_id = '1';
    $levelQuery = "SELECT `level`, level_id FROM `level` WHERE limit_from < $karmaPoints ORDER BY `level` DESC LIMIT 1";
    $levelResult = mysqli_query($conn, $levelQuery);
    if ($levelRow = mysqli_fetch_assoc($levelResult)) {
        $level = $levelRow['level'];
        $level_id = $levelRow['level_id'];
    }
    mysqli_free_result($levelResult);

    if ($karmaPoints !== null) {
        $data = [
            'status' => 201,
            'message' => 'User Details Fetched Successfully',
            'data' => [
                'user_id' => $user_id,
                'karma_points' => $karmaPoints,
                'surveyCreateCountLive' => $surveyCreateCountLive,
                'surveyCreateCountClose' => $surveyCreateCountClose,
                'surveyFillCount' => $surveyFillCount,
                'level' => $level,
                'level_id' => $level_id
            ]
        ];
        return json_encode($data);
    } else {
        $data = [
            'status' => 404,
            'message' => 'No User Details Found',
            'data' => []
        ];
        header("HTTP/1.0 404 No User Details Found");
        return json_encode($data);
    }
}

function possibleResponseCount($gender, $region_id, $profession_id,$sub_profession_id)
{
    global $conn;

    // Get possible response total count
    $possibleResponseTotalCountQuery = "SELECT COUNT(*) FROM user_registration ur JOIN untitled u ON ur.user_id=u.user_id  WHERE  ur.del_flag=1 AND u.category=2 AND  1=1";
    if ($gender) {
        $possibleResponseTotalCountQuery .= " AND gender = $gender";
    }
    if ($region_id) {
        $possibleResponseTotalCountQuery .= " AND region_id = $region_id";
    }
    if ($profession_id) {
        $possibleResponseTotalCountQuery .= " AND profession_id = $profession_id";
    }
    if ($sub_profession_id) {
        $possibleResponseTotalCountQuery .= " AND sub_profession_id = $sub_profession_id";
    }
    // Execute query for gender count
    $genderCount = 0;
    $genderQuery = "SELECT COUNT(*) FROM user_registration ur JOIN untitled u ON ur.user_id=u.user_id  WHERE  ur.del_flag=1 AND u.category=2 AND ur.gender=?";
    $genderStmt = mysqli_prepare($conn, $genderQuery);
    mysqli_stmt_bind_param($genderStmt, "i", $gender);
    mysqli_stmt_execute($genderStmt);
    mysqli_stmt_bind_result($genderStmt, $genderCount);
    mysqli_stmt_fetch($genderStmt);
    mysqli_stmt_close($genderStmt);

    // Execute query for region count
    $regionCount = 0;
    $regionQuery = "SELECT COUNT(*) FROM user_registration ur JOIN untitled u ON ur.user_id=u.user_id  WHERE  ur.del_flag=1 AND u.category=2 AND ur.region_id=?";
    $regionStmt = mysqli_prepare($conn, $regionQuery);
    mysqli_stmt_bind_param($regionStmt, "i", $region_id);
    mysqli_stmt_execute($regionStmt);
    mysqli_stmt_bind_result($regionStmt, $regionCount);
    mysqli_stmt_fetch($regionStmt);
    mysqli_stmt_close($regionStmt);

    // Execute query for profession count
    $professionCount = 0;
    $professionQuery = "SELECT COUNT(*) FROM user_registration ur JOIN untitled u ON ur.user_id=u.user_id  WHERE  ur.del_flag=1 AND u.category=2 AND ur.profession_id=?";
    $professionStmt = mysqli_prepare($conn, $professionQuery);
    mysqli_stmt_bind_param($professionStmt, "i", $profession_id);
    mysqli_stmt_execute($professionStmt);
    mysqli_stmt_bind_result($professionStmt, $professionCount);
    mysqli_stmt_fetch($professionStmt);
    mysqli_stmt_close($professionStmt);

     // Execute query for sub profession count
     $subProfessionCount = 0;
     $subProfessionQuery = "SELECT COUNT(*) FROM user_registration ur JOIN untitled u ON ur.user_id=u.user_id  WHERE  ur.del_flag=1 AND u.category=2 AND ur.sub_profession_id=?";
     $subProfessionStmt = mysqli_prepare($conn, $subProfessionQuery);
     mysqli_stmt_bind_param($subProfessionStmt, "i", $sub_profession_id);
     mysqli_stmt_execute($subProfessionStmt);
     mysqli_stmt_bind_result($subProfessionStmt, $subProfessionCount);
     mysqli_stmt_fetch($subProfessionStmt);
     mysqli_stmt_close($subProfessionStmt);

    // Execute query for total count
    $totalCount = 0;
    $totalStmt = mysqli_query($conn, $possibleResponseTotalCountQuery);
    $totalRow = mysqli_fetch_array($totalStmt);
    $totalCount = $totalRow[0];

    $percentageQuery = "SELECT MAX(percentage) AS percentage FROM percentage WHERE del_flag = 1";
    $percentageResult = mysqli_query($conn, $percentageQuery);
    
   
    
        if (mysqli_num_rows($percentageResult) > 0) {
            $percentageRow = mysqli_fetch_assoc($percentageResult);
            $maxPercentage=$percentageRow['percentage'];
            $possibleResCount = round(($percentageRow['percentage'] / 100) * $totalCount);
        }
    

    // Prepare response data
    $data = [
        'status' => 201,
        'message' => 'Possible response counts fetched successfully',
        'data' => [
            'genderCount' => $genderCount,
            'regionCount' => $regionCount,
            'totalCount' => $totalCount,
            'possibleResCount'=>$possibleResCount,
            'maxPercentage'=>$maxPercentage,
            'professionCount'=>$professionCount,
            'subProfessionCount'=>$subProfessionCount,
        ]
    ];

    return json_encode($data);
}
