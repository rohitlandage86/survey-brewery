<?php
require '../../connection/conn.php';
function error422($message)
{
    $data = [
        "status" => 422,
        "message" => $message
    ];
    header("HTTP/1.0 422 unprocessable Entity");
    echo json_encode($data);
    exit();
}

function fillSurveyHeader($inputData)
{
    global $conn;
    try {
        // Start transaction
        mysqli_begin_transaction($conn);

        // Retrieve input data
        $survey_id = isset($inputData['survey_id']) ? mysqli_real_escape_string($conn, $inputData['survey_id']) : '';
        $user_id = isset($inputData['user_id']) ? mysqli_real_escape_string($conn, $inputData['user_id']) : '';
        $level_id = isset($inputData['level_id']['levelId']) ? mysqli_real_escape_string($conn, $inputData['level_id']['levelId']) : '';
        $questions = isset($inputData['questions']) ? $inputData['questions'] : [];

        // Retrieve the category for the user_id from the related table
        $categoryQuery = "SELECT category FROM untitled WHERE user_id = '$user_id'";
        $categoryResult = mysqli_query($conn, $categoryQuery);
        if ($categoryResult && mysqli_num_rows($categoryResult) > 0) {
            $row = mysqli_fetch_assoc($categoryResult);
            $category = $row['category'];
        }

        // Validate required fields
        if (empty(trim($survey_id))) {
            return error422('Survey id is required');
        } elseif (empty(trim($user_id))) {
            return error422('User id is required');
        } elseif (empty(trim($level_id))) {
            return error422('Level is required');
        } elseif (empty($questions) || !is_array($questions)) {
            return error422('Questions are required and should be an array');
        } else {
            foreach ($questions as $question) {
                if (empty(trim($question['survey_question_id']))) {
                    return error422('Question id is required');
                }
            }
        }
        $query = "SELECT * FROM survey_header WHERE survey_id='$survey_id'";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) > 0) {
            $row = mysqli_fetch_assoc($result);
            $surveyStatus = $row['status'];

            if ($surveyStatus == 0) {
                // Survey is disabled
                return error422('Not Allowed');
            }
        }

        // Check if the survey is disable or not
        $query = "SELECT * FROM survey_header WHERE survey_title='$survey_id' ";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) > 0) {
            return error422('Not Allow');
        }

        // Insert fill_survey_header record
        $query = "INSERT INTO fill_survey_header (survey_id, user_id) VALUES ('$survey_id', '$user_id')";
        $result = mysqli_query($conn, $query);

        if (!$result) {
            throw new Exception('Failed to insert fill_survey_header record');
        }

        // Get the inserted fill survey ID
        $fillSurveyId = mysqli_insert_id($conn);

        // Insert questions
        foreach ($questions as $question) {
            $survey_question_id = mysqli_real_escape_string($conn, $question['survey_question_id']);
            $survey_question_no = mysqli_real_escape_string($conn, $question['survey_question_no']);

            foreach ($question['values'] as $value) {
                $survey_question_option_id = mysqli_real_escape_string($conn, $value['survey_question_option_id']);
                $value = mysqli_real_escape_string($conn, $value['value']);

                $query = "INSERT INTO fill_survey_question (fill_survey_id, survey_question_id, survey_question_no, value, survey_question_option_id, survey_id, user_id)
                          VALUES ('$fillSurveyId', '$survey_question_id', '$survey_question_no', '$value', '$survey_question_option_id', '$survey_id', '$user_id')";

                $result = mysqli_query($conn, $query);

                if (!$result) {
                    throw new Exception('Failed to insert fill_survey_question record');
                }
            }
        }

        $points = 0; // Define the $points variable before the condition

        if ($category != "1") {
            $queryLevel = "SELECT points_after_fill FROM level WHERE level_id='$level_id' AND del_flag='1'";
            $result = mysqli_query($conn, $queryLevel);

            if ($result && mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_assoc($result);
                $points = $row['points_after_fill'];
                $insertkarmaLog = "INSERT INTO karma_points_log (user_id, level_id, survey_id, points, survey_type,total) VALUES ($user_id, $level_id, $survey_id, '$points', 'fill',0)";
                $result = mysqli_query($conn, $insertkarmaLog);
                $kpl_id = mysqli_insert_id($conn); // Retrieve kpl id from karma points log table
                if (!$result) {
                    throw new Exception('Failed to insert karma_points_log record');
                }
            }
        }

        $query = "SELECT user_id FROM karma_point WHERE user_id = '$user_id'";
        $result = mysqli_query($conn, $query);

        if ($result && mysqli_num_rows($result) > 0) {
            $query = "SELECT total FROM karma_point WHERE user_id = $user_id";
            $query_run = mysqli_query($conn, $query);
            $total = mysqli_fetch_assoc($query_run);
            $tot = $total["total"];
            $closing = (int)$points + $tot;
            //return json_encode($closing);
            $query = "UPDATE karma_point SET opening = $tot, points = '$points', closing = '$closing', total = '$closing' WHERE user_id = $user_id";
            $result = mysqli_query($conn, $query);

            $query = "UPDATE karma_points_log SET  total = '$closing' WHERE kp_id = $kpl_id";
            $result1 = mysqli_query($conn, $query);
            if (!$result) {
                throw new Exception('Failed to update karma_point record');
            }
        } else {
            $insertLevelQuery = "INSERT INTO karma_point (user_id, `opening`, `points`, `closing`, `total`) VALUES (?, 0, ?, ?, ?)";
            $stmt = mysqli_prepare($conn, $insertLevelQuery);
            mysqli_stmt_bind_param($stmt, "iiii", $user_id, $points, $points, $points);
            $result = mysqli_stmt_execute($stmt);
            $query = "UPDATE karma_points_log SET  total = '$points' WHERE kp_id = $kpl_id";
            $result1 = mysqli_query($conn, $query);
            if (!$result) {
                throw new Exception('Failed to insert karma_point record');
            }
        }
        $query = "SELECT COUNT(*) AS fillSurveyCount, sh.possible_res_count FROM fill_survey_header f JOIN survey_header sh ON sh.survey_id = f.survey_id WHERE f.survey_id = $survey_id";
        $result = mysqli_query($conn, $query);

        if (mysqli_num_rows($result) > 0) {
            $row = mysqli_fetch_assoc($result);
            $fillSurveyCount = $row['fillSurveyCount'];
            $possibleResCount = $row['possible_res_count'];

            if ($fillSurveyCount >= $possibleResCount) {
                // Disable survey
                $query = "UPDATE survey_header SET status = 0 WHERE survey_id = $survey_id";
                $result = mysqli_query($conn, $query);
            }
        }


        // Commit transaction
        mysqli_commit($conn);

        $data = [
            'status' => 201,
            'message' => 'The survey has been filled successfully',
        ];
        return json_encode($data);
    } catch (Exception $e) {
        mysqli_rollback($conn);
        $data = [
            'status' => 500,
            'message' => 'An error occurred: ' . $e->getMessage(),
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }
}
function getSurveyFillById($fillSurveyId)
{
    global $conn;

    try {
        // Retrieve survey fill details
        $query = "SELECT fill_survey_header.*, fill_survey_question.*, survey_question_option.option, survey_question.question, survey_header.survey_title
                  FROM fill_survey_header
                  JOIN fill_survey_question ON fill_survey_header.fill_survey_id = fill_survey_question.fill_survey_id
                  JOIN survey_header ON fill_survey_header.survey_id = survey_header.survey_id
                  JOIN survey_question ON fill_survey_question.survey_question_id = survey_question.survey_question_id
                  LEFT JOIN survey_question_option ON fill_survey_question.survey_question_option_id = survey_question_option.survey_question_option_id
                  WHERE fill_survey_header.fill_survey_id = '$fillSurveyId'";
        $result = mysqli_query($conn, $query);


        $query = "SELECT fill_survey_header.";
        if (!$result) {
            throw new Exception('Failed to retrieve survey fill details');
        }

        $fillSurveyDetails = [
            'fill_survey_id' => null,
            'survey_id' => null,
            'survey_title' => null,
            'user_id' => null,
            'cts' => null,
            'mts' => null,
            'del_flag' => null,
            'questions' => []
        ];

        while ($row = mysqli_fetch_assoc($result)) {
            $fillSurveyId = $row['fill_survey_id'];
            $surveyQuestionId = $row['survey_question_id'];

            // Check if the question already exists in the array
            $questionExists = false;
            $questionIndex = -1;
            foreach ($fillSurveyDetails['questions'] as $index => $question) {
                if ($question['survey_question_id'] === $surveyQuestionId) {
                    $questionExists = true;
                    $questionIndex = $index;
                    break;
                }
            }

            // Create a question object
            $question = [
                'fill_survey_question_id' => $row['fill_survey_question_id'],
                'survey_question_id' => $row['survey_question_id'],
                'survey_question_no' => $row['survey_question_no'],
                'question' => $row['question'],
                'values' => [],
            ];

            // Check if the question has a value and survey_question_option_id
            if (isset($row['value']) && isset($row['survey_question_option_id'])) {
                // Add the value and survey_question_option_id to the question object
                $value = [
                    'value' => $row['value'],
                    'survey_question_option_id' => $row['survey_question_option_id'],
                ];

                $question['values'][] = $value;
            }

            if ($questionExists) {
                // If the question exists, add the value to the existing question
                $fillSurveyDetails['questions'][$questionIndex]['values'][] = $value;
            } else {
                // If the question doesn't exist, add the question object to the questions array
                $fillSurveyDetails['questions'][] = $question;
            }

            // Assign survey fill details once
            if (!$fillSurveyDetails['fill_survey_id']) {
                $fillSurveyDetails['fill_survey_id'] = $row['fill_survey_id'];
                $fillSurveyDetails['survey_id'] = $row['survey_id'];
                $fillSurveyDetails['survey_title'] = $row['survey_title'];
                $fillSurveyDetails['user_id'] = $row['user_id'];
                $fillSurveyDetails['cts'] = $row['cts'];
                $fillSurveyDetails['mts'] = $row['mts'];
                $fillSurveyDetails['del_flag'] = $row['del_flag'];
            }
        }

        $data = [
            'status' => 200,
            'message' => 'Survey fill details retrieved successfully',
            'data' => $fillSurveyDetails,
        ];
        return json_encode($data);
    } catch (Exception $e) {
        $data = [
            'status' => 500,
            'message' => 'An error occurred: ' . $e->getMessage(),
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }
}

function getFillSurveyResponseById($surveyId)
{
    global $conn;

    try {
        // Get the total survey fill count
        $totalFillCountQuery = "SELECT COUNT(*) AS survey_fill_count FROM fill_survey_header WHERE survey_id = '$surveyId'";
        $totalFillCountResult = mysqli_query($conn, $totalFillCountQuery);
        $totalFillCountRow = mysqli_fetch_assoc($totalFillCountResult);
        $surveyFillCount = $totalFillCountRow['survey_fill_count'];
        // Get the survey details from survey_header table
        $surveyQuery = "SELECT survey_header.* ,level.level  FROM survey_header JOIN level  ON survey_header.level_id = level.level_id  WHERE survey_id = '$surveyId'";
        $surveyResult = mysqli_query($conn, $surveyQuery);
        $surveyDetails = mysqli_fetch_assoc($surveyResult);
        // Get the start and end dates for the survey based on the survey_id
        $dateQuery = "SELECT start_date, end_date FROM survey_header  WHERE survey_id = '$surveyId'";
        $dateResult = mysqli_query($conn, $dateQuery);
        $dates = mysqli_fetch_assoc($dateResult);
        $startDate = $dates['start_date'];
        $endDate = $dates['end_date'];

        // Create an array of all dates between the start and end date
        $allDates = [];
        $currentDate = strtotime($startDate);
        $endDateTimestamp = strtotime($endDate);

        while ($currentDate <= $endDateTimestamp) {
            $allDates[] = date('Y-m-d', $currentDate);
            $currentDate = strtotime('+1 day', $currentDate);
        }

        // Get the survey fill count by date range
        $dateRangeQuery = "SELECT DATE(cts) AS fill_date, COUNT(user_id) AS survey_fill_count, COUNT(DISTINCT user_id) AS unique_user_fill_count
                           FROM fill_survey_header 
                           WHERE survey_id = '$surveyId' 
                           AND DATE(cts) >= '$startDate' 
                           AND DATE(cts) <= '$endDate' 
                           GROUP BY DATE(cts)";

        $dateRangeResult = mysqli_query($conn, $dateRangeQuery);
        $fillCountByDate = [];

        while ($row = mysqli_fetch_assoc($dateRangeResult)) {
            $fillCountByDate[$row['fill_date']] = [
                'day' => $row['fill_date'],
                'surveyFillCount' => $row['survey_fill_count'], // Total survey responses for the day
                'uniqueUserFillCount' => $row['unique_user_fill_count'], // Number of unique users who filled the survey on the day
            ];
        }

        // Set fill count to 0 for dates when no users filled the survey
        foreach ($allDates as $date) {
            if (!isset($fillCountByDate[$date])) {
                $fillCountByDate[$date] = [
                    'day' => $date,
                    'surveyFillCount' => 0,
                    'uniqueUserFillCount' => 0,
                ];
            }
        }

        // Sort the fill count by date in ascending order
        ksort($fillCountByDate);

        // Get profession fill counts
        $professionFillQuery = "SELECT p.profession, COUNT(DISTINCT f.user_id) AS survey_fill_count
                                FROM fill_survey_header f 
                                JOIN user_registration u ON f.user_id = u.user_id 
                                JOIN profession p ON u.profession_id = p.profession_id 
                                WHERE f.survey_id = '$surveyId' 
                                AND DATE(f.cts) >= '$startDate' 
                                AND DATE(f.cts) <= '$endDate'
                                GROUP BY p.profession";

        $professionFillResult = mysqli_query($conn, $professionFillQuery);
        $professionFills = [];
        while ($row = mysqli_fetch_assoc($professionFillResult)) {
            $professionFills[] = [
                'profession' => $row['profession'],
                'surveyFillCount' => $row['survey_fill_count'],
            ];
        }

        // Get region fill counts
        $regionFillQuery = "SELECT r.region, COUNT(DISTINCT f.user_id) AS survey_fill_count
                FROM fill_survey_header f 
                JOIN user_registration u ON f.user_id = u.user_id 
                JOIN regions r ON u.region_id = r.region_id 
                WHERE f.survey_id = '$surveyId' 
                AND DATE(f.cts) >= '$startDate' 
                AND DATE(f.cts) <= '$endDate'
                GROUP BY r.region";

        $regionFillResult = mysqli_query($conn, $regionFillQuery);
        $regionFills = [];
        while ($row = mysqli_fetch_assoc($regionFillResult)) {
            $regionFills[] = [
                'region' => $row['region'],
                'regionFillCount' => $row['survey_fill_count'],
            ];
        }

        // Get sub profession fill counts
        $subProfessionFillQuery = "SELECT sp.sub_profession, COUNT(DISTINCT f.user_id) AS survey_fill_count
                FROM fill_survey_header f 
                JOIN user_registration u ON f.user_id = u.user_id 
                JOIN sub_profession sp ON u.sub_profession_id = sp.sub_profession_id 
                WHERE f.survey_id = '$surveyId' 
                AND DATE(f.cts) >= '$startDate' 
                AND DATE(f.cts) <= '$endDate'
                GROUP BY sp.sub_profession";

        $subProfessionFillResult = mysqli_query($conn, $subProfessionFillQuery);
        $subProfessionFills = [];
        while ($row = mysqli_fetch_assoc($subProfessionFillResult)) {
            $subProfessionFills[] = [
                'sub_profession' => $row['sub_profession'],
                'subProfessionFillCount' => $row['survey_fill_count'],
            ];
        }
        // Get gender fill counts
        $genderFillQuery = "SELECT g.gender, COUNT(DISTINCT f.user_id) AS survey_fill_count
                FROM fill_survey_header f 
                JOIN user_registration u ON f.user_id = u.user_id 
                JOIN gender g ON u.gender = g.gender_id 
                WHERE f.survey_id = '$surveyId' 
                AND DATE(f.cts) >= '$startDate' 
                AND DATE(f.cts) <= '$endDate'
                GROUP BY g.gender";
        $genderFillResult = mysqli_query($conn, $genderFillQuery);
        $genderFills = [];
        while ($row = mysqli_fetch_assoc($genderFillResult)) {
            $genderFills[] = [
                'gender' => $row['gender'],
                'genderFillCount' => $row['survey_fill_count'],
            ];
        }

        $data = [
            'status' => 200,
            'fill_count_by_date' => array_values($fillCountByDate),
            'start_date' => $startDate,
            'end_date' => $endDate,
            'profession_fills' => $professionFills,
            'regionFills' => $regionFills,
            'subProfessionFills' => $subProfessionFills,
            'genderFills' => $genderFills,
            'survey_details' => $surveyDetails,
            'survey_fill_count' => $surveyFillCount,
        ];
        return json_encode($data);
    } catch (Exception $e) {
        $data = [
            'status' => 500,
            'message' => 'An error occurred: ' . $e->getMessage(),
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }
}


function getUserListSurveyId($surveyId)
{
    global $conn;

    try {
        $query = "SELECT fill_survey_header.*,
                user_registration.user_name,
                user_registration.full_name,
                profession.profession_id,
                profession.profession
            FROM
            fill_survey_header
            JOIN user_registration ON fill_survey_header.user_id = user_registration.user_id
            JOIN profession ON user_registration.profession_id = profession.profession_id
                WHERE fill_survey_header.survey_id='$surveyId'
            ORDER BY
            fill_survey_header.cts ASC";

        $query_run = mysqli_query($conn, $query);

        if (mysqli_num_rows($query_run) > 0) {
            $res = mysqli_fetch_all($query_run, MYSQLI_ASSOC);
            $data = [
                'status' => 201,
                'message' => 'user  list Fetched Successfully',
                'data' => $res
            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 404,
                'data' => [],
                'message' => 'No User Found'
            ];
            header("HTTP/1.0 404 No User Found");
            return json_encode($data);
        }
    } catch (Exception $e) {
        $data = [
            'status' => 500,
            'message' => 'An error occurred: ' . $e->getMessage(),
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }
}
