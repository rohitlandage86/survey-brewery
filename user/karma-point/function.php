<?php
require '../../connection/conn.php';
function error422($message)
{
    $data = [
        "status" => 422,
        "message" => $message
    ];
    header("HTTP/1.0 422 unprocessable Entity");
    echo json_encode($data);
    exit();
}
function getTotalKarmaPointsById($user_id)
{
    global $conn;
    $query = "SELECT total FROM karma_point WHERE user_id = $user_id";
    $query_run = mysqli_query($conn, $query);
    if ($query_run) {
        if (mysqli_num_rows($query_run) > 0) {
            $res = mysqli_fetch_assoc($query_run);
            $data = [
                'status' => 201,
                'message' => 'Total karma points for this user',
                'total' => intval($res['total']) // Convert to integer
            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 201,
                'message' => 'Total karma points for this user not available. Please fill a survey.',
                'total' => 0
            ];
            return json_encode($data);
        }
    } else {
        $data = [
            'status' => 500,
            'message' => 'Internal Server Error',
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }
}




?>