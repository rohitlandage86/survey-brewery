<?php
require '../../../connection/conn.php';
function error422($message)
{
    $data = [
        "status" => 422,
        "message" => $message
    ];
    header("HTTP/1.0 422 unprocessable Entity");
    echo json_encode($data);
    exit();
}
function getAllFillSurveyList1($page,$perPage,$fromDate, $toDate, $user_id){
    global $conn;
    $query = "SELECT
    fill_survey_header.user_id,
    fill_survey_header.survey_id,
    survey_header.survey_title,
    survey_header.end_date,
    survey_header.start_date,
    COUNT(DISTINCT survey_question.survey_question_id) AS questionsCount,
    COUNT(DISTINCT fill_survey_header.user_id) AS usersCount
  FROM
    fill_survey_question
    INNER JOIN fill_survey_header ON fill_survey_header.fill_survey_id = fill_survey_question.fill_survey_id
    INNER JOIN survey_header ON survey_header.survey_id = fill_survey_header.survey_id
    INNER JOIN survey_question ON survey_question.survey_id = survey_header.survey_id
  WHERE
    fill_survey_header.del_flag = '1'";
  
    if ($user_id) {
        $query .= " AND fill_survey_header.user_id = $user_id";
    }
  
    if ($fromDate && $toDate) {
        $query .= " AND survey_header.start_date >= '$fromDate' 
                    AND survey_header.start_date <= '$toDate'";
    }
  
    $query .= " GROUP BY fill_survey_header.user_id,  fill_survey_header.survey_id, survey_header.survey_title, survey_header.end_date,survey_header.start_date";
    $query .=" ORDER BY `fill_survey_header`.`cts` DESC";

    $query_run = mysqli_query($conn, $query);
    if ($query_run) {
        if (mysqli_num_rows($query_run) > 0) {
            $res = mysqli_fetch_all($query_run, MYSQLI_ASSOC);
            $data = [
                'status' => 201,
                'message' => 'Fill survey list fetched successfully',
                'data' => $res
            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 404,
                'data' => [],
                'message' => 'No fill surveys found'
            ];
            header("HTTP/1.0 404 Not Found");
            return json_encode($data);
        }
    } else {
        $data = [
            'status' => 500,
            'message' => 'Internal Server Error',
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }
}

function getAllFillSurveyList($page, $perPage, $fromDate, $toDate, $user_id)
{
    global $conn;
    $query = "SELECT
    fill_survey_header.user_id,
    fill_survey_header.survey_id,
    fill_survey_header.fill_survey_id,
    survey_header.survey_title,
    survey_header.end_date,
    survey_header.start_date,
    COUNT(DISTINCT survey_question.survey_question_id) AS questionsCount,
    COUNT(DISTINCT fill_survey_header.user_id) AS usersCount
  FROM
    fill_survey_question
    INNER JOIN fill_survey_header ON fill_survey_header.fill_survey_id = fill_survey_question.fill_survey_id
    INNER JOIN survey_header ON survey_header.survey_id = fill_survey_header.survey_id
    INNER JOIN survey_question ON survey_question.survey_id = survey_header.survey_id
  WHERE
    fill_survey_header.del_flag = '1'";

    if ($user_id) {
        $query .= " AND fill_survey_header.user_id = $user_id";
    }

    if ($fromDate && $toDate) {
        $query .= " AND survey_header.start_date >= '$fromDate' 
                    AND survey_header.start_date <= '$toDate'";
    }

    $query .= " GROUP BY fill_survey_header.user_id,  fill_survey_header.survey_id, survey_header.survey_title, survey_header.end_date, survey_header.start_date";
    $query .= " ORDER BY `fill_survey_header`.`cts` DESC";

    // Get the total count of fill surveys
    $countQuery = "SELECT COUNT(*) AS total FROM ($query) AS count";
    $countResult = mysqli_query($conn, $countQuery);
    $totalCount = 0;
    if ($countResult) {
        $countRow = mysqli_fetch_assoc($countResult);
        $totalCount = $countRow['total'];
    }

    // Add pagination to the main query
    if ($page !== null && $perPage !== null) {
        $start = ($page - 1) * $perPage;
        $query .= " LIMIT $start, $perPage";
    }

    $query_run = mysqli_query($conn, $query);
    if ($query_run) {
        if (mysqli_num_rows($query_run) > 0) {
            $res = mysqli_fetch_all($query_run, MYSQLI_ASSOC);
            $data = [
                'status' => 201,
                'message' => 'Fill survey list fetched successfully',
                'data' => $res
            ];

            // Add pagination information to the response
            if ($page !== null && $perPage !== null) {
                $data['pagination'] = [
                    'per_page' => $perPage,
                    'total' => $totalCount,
                    'current_page' => $page,
                    'last_page' => ceil($totalCount / $perPage)
                ];
            }

            return json_encode($data);
        } else {
            $data = [
                'status' => 404,
                'data' => [],
                'message' => 'No fill surveys found'
            ];
            header("HTTP/1.0 404 Not Found");
            return json_encode($data);
        }
    } else {
        $data = [
            'status' => 500,
            'message' => 'Internal Server Error',
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }
}




?>