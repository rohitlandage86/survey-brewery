<?php
require '../../../connection/conn.php';
function error422($message)
{
    $data = [
        "status" => 422,
        "message" => $message
    ];
    header("HTTP/1.0 422 unprocessable Entity");
    echo json_encode($data);
    exit();
}

function getAllCreateSurveyList($page, $perPage, $fromDate, $toDate, $user_id)
{
    global $conn;
    $query = "SELECT survey_header.user_id, survey_header.end_date, survey_header.start_date, survey_header.survey_id, survey_header.status, COUNT(survey_question.survey_question_id) AS questionsCount, survey_header.survey_title, user_registration.user_name FROM survey_question INNER JOIN survey_header ON survey_header.survey_id = survey_question.survey_id INNER JOIN user_registration ON user_registration.user_id = survey_header.user_id WHERE survey_header.del_flag = '1'";

    if ($user_id !== null) {
        $query .= " AND survey_header.user_id = $user_id";
    }

    if ($fromDate && $toDate) {
        $query .= " AND survey_header.start_date >= '$fromDate' AND survey_header.start_date <= '$toDate'";
    }

    $query .= " GROUP BY survey_header.survey_id";
    $query .= " ORDER BY `survey_header`.`cts` DESC";

    // Get the total count of surveys
    $countQuery = "SELECT COUNT(*) AS total FROM ($query) AS count";
    $countResult = mysqli_query($conn, $countQuery);
    $totalCount = 0;
    if ($countResult) {
        $countRow = mysqli_fetch_assoc($countResult);
        $totalCount = $countRow['total'];
    }

    // Add pagination to the main query
    if ($page !== null && $perPage !== null) {
        $start = ($page - 1) * $perPage;
        $query .= " LIMIT $start, $perPage";
    }

    $query_run = mysqli_query($conn, $query);
    if ($query_run) {
        if (mysqli_num_rows($query_run) > 0) {
            $res = mysqli_fetch_all($query_run, MYSQLI_ASSOC);
            $data = [
                'status' => 201,
                'message' => 'Survey list fetched successfully',
                'data' => $res
            ];

            // Add pagination information to the response
            if ($page !== null && $perPage !== null) {
                $data['pagination'] = [
                    'per_page' => $perPage,
                    'total' => $totalCount,
                    'current_page' => $page,
                    'last_page' => ceil($totalCount / $perPage)
                ];
            }

            return json_encode($data);
        } else {
            $data = [
                'status' => 404,
                'data' => [],
                'message' => 'No surveys found'
            ];
            header("HTTP/1.0 404 Not Found");
            return json_encode($data);
        }
    } else {
        $data = [
            'status' => 500,
            'message' => 'Internal Server Error',
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }
}
