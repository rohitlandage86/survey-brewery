<?php
require '../../../connection/conn.php';
function error422($message)
{
    $data = [
        'status' => 422,
        'message' => $message,
    ];
    header("HTTP/1.0 422 unprocessable Entity");
    echo json_encode($data);
    exit();
}
function createRegion($inputData)
{
    global $conn;
    $region = mysqli_real_escape_string($conn, trim($inputData['region']));
    $description = mysqli_real_escape_string($conn, $inputData['description']);
    $user_id = mysqli_real_escape_string($conn, $inputData['user_id']);

    if (empty(trim($region))) {
        return error422('Enter region');
    } elseif (empty(trim($user_id))) {
        return error422('user id is required');
    } else {
        // Check if the regions already exists for the user
        $query = "SELECT * FROM regions WHERE user_id='$user_id' AND region='$region' AND del_flag=1";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) > 0) {
            return error422('This region already exists ');
        }
        $query = "INSERT INTO regions (region,description,user_id) VALUES ('$region','$description',$user_id)";
        $result = mysqli_query($conn, $query);
        if ($result) {
            $data = [
                'status' => 201,
                'message' => 'The region has created successfully'

            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 500,
                'message' => 'Internal Server Error',
            ];
            header("HTTP/1.0 500 Internal Server Error");
            return json_encode($data);
        }
    }
}
function getAllRegionsList($page, $perPage)
{
    global $conn;
    $query = "SELECT regions.*, user_registration.user_name,user_registration.full_name FROM regions  LEFT JOIN user_registration ON regions.user_id = user_registration.user_id WHERE  regions.del_flag = '1' ORDER BY `regions`.`cts` DESC";
    // Get the total count of surveys
    $countQuery = "SELECT COUNT(*) AS total FROM ($query) AS count";
    $countResult = mysqli_query($conn, $countQuery);
    $totalCount = 0;
    if ($countResult) {
        $countRow = mysqli_fetch_assoc($countResult);
        $totalCount = $countRow['total'];
    }

    // Add pagination to the main query
    if ($page !== null && $perPage !== null) {
        $start = ($page - 1) * $perPage;
        $query .= " LIMIT $start, $perPage";
    }
    $query_run = mysqli_query($conn, $query);

    if ($query_run) {
        if (mysqli_num_rows($query_run) > 0) {
            $res = mysqli_fetch_all($query_run, MYSQLI_ASSOC);
            $data = [
                'status' => 201,
                'message' => 'Regions list Fetched Successfully',
                'data' => $res
            ];
            // Add pagination information to the response
            if ($page !== null && $perPage !== null) {
                $data['pagination'] = [
                    'per_page' => $perPage,
                    'total' => $totalCount,
                    'current_page' => $page,
                    'last_page' => ceil($totalCount / $perPage)
                ];
            }
            return json_encode($data);
        } else {
            $data = [
                'status' => 404,
                'data' => [],
                'message' => 'No Regions Found'
            ];
            header("HTTP/1.0 404 No Regions Found");
            return json_encode($data);
        }
    } else {
        $data = [
            'status' => 500,
            'message' => 'Internal Server Error',
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }
}
function updateRegion($inputData, $id)
{
    global $conn;
    $region = mysqli_real_escape_string($conn, trim($inputData['region']));
    $description = mysqli_real_escape_string($conn, $inputData['description']);
    $user_id = mysqli_real_escape_string($conn, $inputData['user_id']);

    if (empty(trim($region))) {
        return error422('Enter region');
    } elseif (empty(trim($user_id))) {
        return error422('user id is required');
    } else {
        $regionCheckQuery = "SELECT region_id FROM regions WHERE region = '$region' AND region_id <> $id AND  user_id='$user_id' AND del_flag=1";
        $regionCheckResult = mysqli_query($conn, $regionCheckQuery);
        if (mysqli_num_rows($regionCheckResult) > 0) {
            return error422('This region already exists');
        }
        $query = "UPDATE regions SET region = '$region', description = '$description', user_id = $user_id WHERE region_id = $id";
        $result = mysqli_query($conn, $query);
        if ($result) {
            $data = [
                'status' => 201,
                'message' => 'The region has been updated successfully'
            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 500,
                'message' => 'Internal Server Error'
            ];
            header("HTTP/1.0 500 Internal Server Error");
            return json_encode($data);
        }
    }
}
function deleteRegion($id)
{
    global $conn;
    if (empty(trim($id))) {
        return [
            'status' => 422,
            'message' => 'Id Not Found In URL'
        ];
    } else {
        $query = "UPDATE regions SET del_flag = '0' WHERE region_id='$id'";
        $result = mysqli_query($conn, $query);

        if ($result) {
            $data = [
                'status' => 201,
                'message' => 'The region has been deleted successfully'
            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 500,
                'message' => 'Internal Server Error'
            ];
            header("HTTP/1.0 500 Internal Server Error");
            return json_encode($data);
        }
    }
}
