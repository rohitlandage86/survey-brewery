<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET');
header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');
include('function.php');
$requestMethod = $_SERVER["REQUEST_METHOD"];
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    http_response_code(200);
    exit();
}

if ($requestMethod == "GET") {
    $page=isset($_GET['page'])? $_GET['page']:null;
    $perPage=isset($_GET['perPage'])?$_GET['perPage']:null;
    $getAllSubProfessionsList = getAllSubProfessionsList($page,$perPage);
    echo $getAllSubProfessionsList;
} else {
    $data = [
        'status' => 405,
        'message' => $requestMethod . 'Method Not Allowed'
    ];
    header("HTTP/1.0 405 Method Not Allowed");
    echo json_encode($data);
}
