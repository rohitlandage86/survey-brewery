<?php
require '../../../connection/conn.php';
function error422($message)
{
    $data = [
        'status' => 422,
        'message' => $message,
    ];
    header("HTTP/1.0 422 unprocessable Entity");
    echo json_encode($data);
    exit();
}
function createSubProfession($inputData)
{
    global $conn;
    $sub_profession = mysqli_real_escape_string($conn, trim($inputData['sub_profession']));
    $profession_id = mysqli_real_escape_string($conn, $inputData['profession_id']);
    $description = mysqli_real_escape_string($conn, $inputData['description']);
    $user_id = mysqli_real_escape_string($conn, $inputData['user_id']);
    if (empty(trim($profession_id))) {
        return error422('profession is required');
    } elseif (empty(trim($sub_profession))) {
        return error422('Sub profession is required');
    } elseif (empty(trim($user_id))) {
        return error422('user id is required');
    } else {
        // Check if the sub_profession already exists 
        $query = "SELECT * FROM sub_profession WHERE user_id='$user_id' AND sub_profession='$sub_profession' AND del_flag=1";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) > 0) {
            return error422('This sub profession already exists ');
        }
        $query = "INSERT INTO sub_profession (sub_profession,profession_id,description,user_id) VALUES ('$sub_profession','$profession_id','$description',$user_id)";
        $result = mysqli_query($conn, $query);
        if ($result) {
            $data = [
                'status' => 201,
                'message' => 'The sub profession has created successfully'

            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 500,
                'message' => 'Internal Server Error',
            ];
            header("HTTP/1.0 500 Internal Server Error");
            return json_encode($data);
        }
    }
}
function getAllSubProfessionsList($page, $perPage)
{
    global $conn;
    $query = "SELECT sub_profession.*, user_registration.user_name, user_registration.full_name,profession.profession
    FROM sub_profession 
    JOIN user_registration ON sub_profession.user_id = user_registration.user_id  
    JOIN profession ON sub_profession.profession_id = profession.profession_id 
    WHERE sub_profession.del_flag = '1' ORDER BY `sub_profession`.`cts` DESC";
    // Get the total count of surveys
    $countQuery = "SELECT COUNT(*) AS total FROM ($query) AS count";
    $countResult = mysqli_query($conn, $countQuery);
    $totalCount = 0;
    if ($countResult) {
        $countRow = mysqli_fetch_assoc($countResult);
        $totalCount = $countRow['total'];
    }

    // Add pagination to the main query
    if ($page !== null && $perPage !== null) {
        $start = ($page - 1) * $perPage;
        $query .= " LIMIT $start, $perPage";
    }
    $query_run = mysqli_query($conn, $query);

    if ($query_run) {
        if (mysqli_num_rows($query_run) > 0) {
            $res = mysqli_fetch_all($query_run, MYSQLI_ASSOC);
            $data = [
                'status' => 201,
                'message' => 'Sub Profession list Fetched Successfully',
                'data' => $res
            ];
            // Add pagination information to the response
            if ($page !== null && $perPage !== null) {
                $data['pagination'] = [
                    'per_page' => $perPage,
                    'total' => $totalCount,
                    'current_page' => $page,
                    'last_page' => ceil($totalCount / $perPage)
                ];
            }

            return json_encode($data);
        } else {
            $data = [
                'status' => 404,
                'data' => [],
                'message' => 'No Sub Profession Found'
            ];
            header("HTTP/1.0 404 No Sub Profession Found");
            return json_encode($data);
        }
    } else {
        $data = [
            'status' => 500,
            'message' => 'Internal Server Error',
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }
}
function updateSubProfession($inputData, $id)
{
    global $conn;
    $sub_profession = mysqli_real_escape_string($conn, trim($inputData['sub_profession']));
    $profession_id = mysqli_real_escape_string($conn, $inputData['profession_id']);
    $description = mysqli_real_escape_string($conn, $inputData['description']);
    $user_id = mysqli_real_escape_string($conn, $inputData['user_id']);

    if (empty(trim($profession_id))) {
        return error422('profession is required');
    } elseif (empty(trim($sub_profession))) {
        return error422('Sub profession is required');
    } elseif (empty(trim($user_id))) {
        return error422('user id is required');
    } else {
        $subProfessionCheckQuery = "SELECT sub_profession_id FROM sub_profession WHERE sub_profession = '$sub_profession' AND sub_profession_id <> $id AND  user_id='$user_id' AND del_flag=1";
        $sub_professionCheckResult = mysqli_query($conn, $subProfessionCheckQuery);
        if (mysqli_num_rows($sub_professionCheckResult) > 0) {
            return error422('This sub profession already exists');
        }
        $query = "UPDATE sub_profession SET profession_id = '$profession_id',sub_profession = '$sub_profession', description = '$description',  user_id = $user_id WHERE sub_profession_id = $id";
        $result = mysqli_query($conn, $query);
        if ($result) {
            $data = [
                'status' => 201,
                'message' => 'The sub profession has been updated successfully'
            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 500,
                'message' => 'Internal Server Error'
            ];
            header("HTTP/1.0 500 Internal Server Error");
            return json_encode($data);
        }
    }
}
function deleteSubProfession($id)
{
    global $conn;
    if (empty(trim($id))) {
        return [
            'status' => 422,
            'message' => 'Id Not Found In URL'
        ];
    } else {
        $query = "UPDATE sub_profession SET del_flag = '0' WHERE sub_profession_id='$id'";
        $result = mysqli_query($conn, $query);

        if ($result) {
            $data = [
                'status' => 201,
                'message' => 'The sub-profession has been deleted successfully'
            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 500,
                'message' => 'Internal Server Error'
            ];
            header("HTTP/1.0 500 Internal Server Error");
            return json_encode($data);
        }
    }
}


function subProfessionByProfessionId($id, $user_id)
{
    global $conn;
    $query = "SELECT sub_profession.*, user_registration.user_name, user_registration.full_name, profession.profession_id,profession.profession 
    FROM sub_profession 
    LEFT JOIN user_registration ON sub_profession.user_id = user_registration.user_id 
    JOIN profession ON sub_profession.profession_id = profession.profession_id 
    WHERE  sub_profession.profession_id=$id AND sub_profession.del_flag = '1'";
    // sub_profession.user_id = $user_id AND
    $query_run = mysqli_query($conn, $query);

    if ($query_run) {
        if (mysqli_num_rows($query_run) > 0) {
            $res = mysqli_fetch_all($query_run, MYSQLI_ASSOC);
            $data = [
                'status' => 201,
                'message' => 'Sub Profession list Fetched Successfully',
                'data' => $res
            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 404,
                'data' => [],
                'message' => 'No Sub Profession Found'
            ];
            header("HTTP/1.0 404 No Sub Profession Found");
            return json_encode($data);
        }
    } else {
        $data = [
            'status' => 500,
            'message' => 'Internal Server Error',
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }
}
