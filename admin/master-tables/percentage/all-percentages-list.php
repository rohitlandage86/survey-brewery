<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: GET');
header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');
include('function.php');
$requestMethod = $_SERVER["REQUEST_METHOD"];
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    http_response_code(200);
    exit();
}

if ($requestMethod=="GET") {
    // Check if Authorization header is present
    $logged_user_id = isset($_GET['logged_user_id']) ? $_GET['logged_user_id'] : null;
    $headers = apache_request_headers();
    if (!isset($headers['Authorization'])) {
        $data = [
            'status' => 401,
            'message' => 'Authorization header is missing',
        ];
        http_response_code(401);
        echo json_encode($data);
        exit();
    }

    // Get the token from Authorization header
    $authHeader = $headers['Authorization'];
    $token = str_replace('Bearer ', '', $authHeader);
    // Check if the token is valid for the given user
    $isValidToken = verifyToken($logged_user_id, $token);
    if ($isValidToken) {
        // Call the function to  perform authorized action
        $user_id=isset($_GET['user_id'])?$_GET['user_id']:null;
        $page=isset($_GET['page'])? $_GET['page']:null;
        $perPage=isset($_GET['perPage'])?$_GET['perPage']:null;
        $getAllPercentagesList=getAllPercentagesList($page,$perPage,$user_id);
        echo $getAllPercentagesList;
    } else {
        $data = [
            'status' => 401,
            'message' => 'Invalid token',
        ];
        http_response_code(401);
        echo json_encode($data);
    }
    
}else{
    $data=[
        'status'=>405,
        'message'=> $requestMethod.'Method Not Allowed'
    ];
    header("HTTP/1.0 405 Method Not Allowed");
    echo json_encode($data);
}

/**
 * Verify if the token is valid for the given user
 * @param string $logged_user_id
 * @param string $token
 * @return bool
 */
function verifyToken($logged_user_id, $token)
{
    global $conn;
    $query = "SELECT * FROM jeton WHERE jeton = '$token' AND utilisatrice = '$logged_user_id'";
    $result = mysqli_query($conn, $query);
    return mysqli_num_rows($result) > 0;
}

?>