<?php
require '../../../connection/conn.php';
function error422($message)
{
    $data = [
        'status' => 422,
        'message' => $message,
    ];
    header("HTTP/1.0 422 unprocessable Entity");
    echo json_encode($data);
    exit();
}
function createPercentage($inputData)
{
    global $conn;
    $percentage = mysqli_real_escape_string($conn, trim($inputData['percentage']));
    $user_id = mysqli_real_escape_string($conn, $inputData['user_id']);
    if (empty(trim($percentage))) {
        return error422('Enter percentage');
    } elseif (empty(trim($user_id))) {
        return error422('user id is required');
    } else {
        // Check if the percentage already exists for the user
        $query = "SELECT * FROM `percentage` WHERE user_id='$user_id' AND `percentage`='$percentage' AND del_flag=1";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) > 0) {
            return error422('This percentage already exists ');
        }
        $query = "INSERT INTO `percentage` (`percentage`,`user_id`) VALUES ('$percentage','$user_id')";
        $result = mysqli_query($conn, $query);
        if ($result) {
            $data = [
                'status' => 201,
                'message' => 'The percentage has created successfully'

            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 500,
                'message' => 'Internal Server Error',
            ];
            header("HTTP/1.0 500 Internal Server Error");
            return json_encode($data);
        }
    }
}



function getAllPercentagesList($page, $perPage, $user_id)
{
    global $conn;

    $query = "SELECT
                percentage.*,
                user_registration.user_name,
                user_registration.full_name
            FROM
                percentage
            JOIN
                user_registration ON percentage.user_id = user_registration.user_id
            ORDER BY
                percentage.percentage_id ASC";
    // Get the total count of surveys
    $countQuery = "SELECT COUNT(*) AS total FROM ($query) AS count";
    $countResult = mysqli_query($conn, $countQuery);
    $totalCount = 0;
    if ($countResult) {
        $countRow = mysqli_fetch_assoc($countResult);
        $totalCount = $countRow['total'];
    }

    // Add pagination to the main query
    if ($page !== null && $perPage !== null) {
        $start = ($page - 1) * $perPage;
        $query .= " LIMIT $start, $perPage";
    }

    $query_run = mysqli_query($conn, $query);

    if ($query_run) {
        if (mysqli_num_rows($query_run) > 0) {
            $res = mysqli_fetch_all($query_run, MYSQLI_ASSOC);
            $data = [
                'status' => 201,
                'message' => 'Percentages list Fetched Successfully',
                'data' => $res
            ];
            // Add pagination information to the response
            if ($page !== null && $perPage !== null) {
                $data['pagination'] = [
                    'per_page' => $perPage,
                    'total' => $totalCount,
                    'current_page' => $page,
                    'last_page' => ceil($totalCount / $perPage)
                ];
            }
            return json_encode($data);
        } else {
            $data = [
                'status' => 404,
                'data' => [],
                'message' => 'No percentage Found'
            ];
            header("HTTP/1.0 404 No percentage Found");
            return json_encode($data);
        }
    } else {
        $data = [
            'status' => 500,
            'message' => 'Internal Server Error',
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }
}








function updatePercentage($inputData, $id)
{
    global $conn;
    $percentage = mysqli_real_escape_string($conn, trim($inputData['percentage']));
    $user_id = mysqli_real_escape_string($conn, $inputData['user_id']);
    if (empty(trim($percentage))) {
        return error422('Enter percentage');
    } elseif (empty(trim($user_id))) {
        return error422('user id is required');
    } else {
        $percentageCheckQuery = "SELECT percentage FROM percentage WHERE percentage = '$percentage' AND percentage <> $id AND  user_id='$user_id' AND del_flag=1";
        $percentageCheckResult = mysqli_query($conn, $percentageCheckQuery);
        if (mysqli_num_rows($percentageCheckResult) > 0) {
            return error422('This percentage already exists');
        }
        $query = "UPDATE percentage SET percentage = '$percentage',  user_id = $user_id WHERE percentage_id = $id";
        $result = mysqli_query($conn, $query);
        if ($result) {
            $data = [
                'status' => 201,
                'message' => 'The percentage has been updated successfully'
            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 500,
                'message' => 'Internal Server Error'
            ];
            header("HTTP/1.0 500 Internal Server Error");
            return json_encode($data);
        }
    }
}

//set percentage enable or disable 
function percentageEnableDisable($status, $id)
{
    global $conn;
    if ($status != 1 && $status != 0) {
        return error422('Invalid status');
    }
    // Check if the percentage exists in the percentage table
    $query = "SELECT * FROM percentage WHERE percentage_id = '$id'";
    $result = mysqli_query($conn, $query);
    if (mysqli_num_rows($result) == 0) {
        return error422('percentage not found');
    }

    // Begin transaction
    mysqli_autocommit($conn, false);

    try {
        // enable or disable percentage
        $query = "UPDATE percentage SET del_flag=$status
                    WHERE percentage_id = '$id'";
        $result = mysqli_query($conn, $query);
        // Commit the transaction if all steps succeed
        mysqli_commit($conn);
        mysqli_autocommit($conn, true);
        // Return the percentage ID and success message
        $message = ($status == 1) ? 'enable' : 'disable';
        $data = [
            'status' => 200,
            'message' => 'Percentage ' . $message,
        ];
        return json_encode($data);
    } catch (Exception $e) {
        // Rollback the transaction if any step fails
        mysqli_rollback($conn);
        mysqli_autocommit($conn, true);

        // Handle the exception
        return error422($e->getMessage());
    }
}
