<?php
require '../../../connection/conn.php';
function error422($message)
{
    $data = [
        'status' => 422,
        'message' => $message,
    ];
    header("HTTP/1.0 422 unprocessable Entity");
    echo json_encode($data);
    exit();
}
function createQuestionType($inputData)
{
    global $conn;
    $question_type = mysqli_real_escape_string($conn, trim($inputData['question_type']));
    $description = mysqli_real_escape_string($conn, $inputData['description']);
    $user_id = mysqli_real_escape_string($conn, $inputData['user_id']);
    $input_type = mysqli_real_escape_string($conn, $inputData['input_type']);

    if (empty(trim($question_type))) {
        return error422('Enter question type');
    } elseif (empty(trim($user_id))) {
        return error422('User id is required');
    } elseif (empty(trim($input_type))) {
        return error422('Input type is required');
    } else {
        // Check if the question_type already exists for the user
        $query = "SELECT * FROM question_type WHERE user_id='$user_id' AND question_type='$question_type' AND del_flag=1";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) > 0) {
            return error422('This question type already exists ');
        }
        $query = "INSERT INTO question_type (question_type,input_type,description,user_id) VALUES ('$question_type','$input_type','$description',$user_id)";
        $result = mysqli_query($conn, $query);
        if ($result) {
            $data = [
                'status' => 201,
                'message' => 'The question type has created successfully'

            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 500,
                'message' => 'Internal Server Error',
            ];
            header("HTTP/1.0 500 Internal Server Error");
            return json_encode($data);
        }
    }
}
function getAllQuestionsTypeList($page, $perPage)
{
    global $conn;
    $query = "SELECT question_type.*, user_registration.user_name,user_registration.full_name FROM question_type JOIN user_registration ON question_type.user_id = user_registration.user_id WHERE question_type.del_flag = '1' ORDER BY `question_type`.`cts` DESC";
    // Get the total count of surveys
    $countQuery = "SELECT COUNT(*) AS total FROM ($query) AS count";
    $countResult = mysqli_query($conn, $countQuery);
    $totalCount = 0;
    if ($countResult) {
        $countRow = mysqli_fetch_assoc($countResult);
        $totalCount = $countRow['total'];
    }

    // Add pagination to the main query
    if ($page !== null && $perPage !== null) {
        $start = ($page - 1) * $perPage;
        $query .= " LIMIT $start, $perPage";
    }

    $query_run = mysqli_query($conn, $query);

    if ($query_run) {
        if (mysqli_num_rows($query_run) > 0) {
            $res = mysqli_fetch_all($query_run, MYSQLI_ASSOC);
            $data = [
                'status' => 201,
                'message' => 'Question Type list Fetched Successfully',
                'data' => $res,
            ];
            // Add pagination information to the response
            if ($page !== null && $perPage !== null) {
                $data['pagination'] = [
                    'per_page' => $perPage,
                    'total' => $totalCount,
                    'current_page' => $page,
                    'last_page' => ceil($totalCount / $perPage)
                ];
            }
            return json_encode($data);
        } else {
            $data = [
                'status' => 404,
                'data' => [],
                'message' => 'No Question Type Found'
            ];
            header("HTTP/1.0 404 No Question Type Found");
            return json_encode($data);
        }
    } else {
        $data = [
            'status' => 500,
            'message' => 'Internal Server Error',
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }
}
function updateQuestionType($inputData, $id)
{
    global $conn;
    $question_type = mysqli_real_escape_string($conn, trim($inputData['question_type']));
    $description = mysqli_real_escape_string($conn, $inputData['description']);
    $user_id = mysqli_real_escape_string($conn, $inputData['user_id']);
    $input_type = mysqli_real_escape_string($conn, $inputData['input_type']);

    if (empty(trim($question_type))) {
        return error422('Enter Question type');
    } elseif (empty(trim($user_id))) {
        return error422('User id is required');
    } elseif (empty(trim($input_type))) {
        return error422('Input type is required');
    } else {
        $questionTypeCheckQuery = "SELECT question_type_id FROM question_type WHERE question_type = '$question_type' AND question_type_id <> $id AND  user_id='$user_id' AND del_flag=1";
        $questionTypeCheckResult = mysqli_query($conn, $questionTypeCheckQuery);
        if (mysqli_num_rows($questionTypeCheckResult) > 0) {
            return error422('This Question Type already exists');
        }
        $query = "UPDATE question_type SET question_type = '$question_type', input_type = '$input_type',description = '$description',  user_id = $user_id WHERE question_type_id = $id";
        $result = mysqli_query($conn, $query);
        if ($result) {
            $data = [
                'status' => 201,
                'message' => 'The Question type has been updated successfully'
            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 500,
                'message' => 'Internal Server Error'
            ];
            header("HTTP/1.0 500 Internal Server Error");
            return json_encode($data);
        }
    }
}
function deleteQuestionType($id)
{
    global $conn;
    if (empty(trim($id))) {
        return [
            'status' => 422,
            'message' => 'Id Not Found In URL'
        ];
    } else {
        $query = "UPDATE question_type SET del_flag = '0' WHERE question_type_id='$id'";
        $result = mysqli_query($conn, $query);

        if ($result) {
            $data = [
                'status' => 201,
                'message' => 'The question type has been deleted successfully'
            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 500,
                'message' => 'Internal Server Error'
            ];
            header("HTTP/1.0 500 Internal Server Error");
            return json_encode($data);
        }
    }
}
?>