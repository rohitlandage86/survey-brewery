<?php
require '../../../connection/conn.php';
function error422($message)
{
    $data = [
        'status' => 422,
        'message' => $message,
    ];
    header("HTTP/1.0 422 unprocessable Entity");
    echo json_encode($data);
    exit();
}
function createLevel($inputData)
{
    global $conn;
    $level = mysqli_real_escape_string($conn, trim($inputData['level']));
    $description = mysqli_real_escape_string($conn, $inputData['description']);
    $points_after_fill = mysqli_real_escape_string($conn, $inputData['points_after_fill']);
    $points_after_create = mysqli_real_escape_string($conn, $inputData['points_after_create']);
    $limit_from = mysqli_real_escape_string($conn, $inputData['limit_from']);
    $limit_to = mysqli_real_escape_string($conn, $inputData['limit_to']);
    $user_id = mysqli_real_escape_string($conn, $inputData['user_id']);

    if (empty(trim($level))) {
        return error422('Enter level');
    } elseif (empty(trim($user_id))) {
        return error422('user id is required');
    } elseif ($limit_from=='') {
        return error422('Enter from limit');
    }  elseif ($limit_to=='') {
        return error422('Enter to limit');
    }  elseif (empty(trim($points_after_fill))) {
        return error422('Enter points after fill');
    } elseif (empty(trim($points_after_create))) {
        return error422('Enter points after create');
    } else {
        // Check if the level already exists for the user
        $query = "SELECT * FROM level WHERE user_id='$user_id' AND level='$level' AND del_flag=1";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) > 0) {
            return error422('This level already exists ');
        }
        $query = "INSERT INTO level (level,description,points_after_fill,points_after_create,limit_from,limit_to,user_id) VALUES ('$level','$description','$points_after_fill','$points_after_create','$limit_from','$limit_to',$user_id)";
        $result = mysqli_query($conn, $query);
        if ($result) {
            $data = [
                'status' => 201,
                'message' => 'The Level has created successfully'

            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 500,
                'message' => 'Internal Server Error',
            ];
            header("HTTP/1.0 500 Internal Server Error");
            return json_encode($data);
        }
    }
}
function getAllAdminLevelsList($page,$perPage,$user_id = null)
{
    global $conn;

    $query = "SELECT
                level.*,
                user_registration.user_name,
                user_registration.full_name,
                COUNT(survey_header.level_id) AS surveyCount,
                IFNULL(filled_surveys.filledCount, 0) AS filledCount,
                (COUNT(survey_header.level_id) - IFNULL(filled_surveys.filledCount, 0)) AS remainingCount
            FROM
                level
            LEFT JOIN
                survey_header ON survey_header.level_id = level.level_id
            JOIN
                user_registration ON level.user_id = user_registration.user_id
            LEFT JOIN
                (
                    SELECT
                        survey_header.level_id,
                        COUNT(fill_survey_header.survey_id) AS filledCount
                    FROM
                        fill_survey_header
                    JOIN
                        survey_header ON fill_survey_header.survey_id = survey_header.survey_id
                    WHERE
                        fill_survey_header.user_id = " . ($user_id ? $user_id : "NULL") . "
                    GROUP BY
                        survey_header.level_id
                ) AS filled_surveys ON filled_surveys.level_id = level.level_id
            WHERE
                level.del_flag = '1'
            GROUP BY
                level.level_id
            ORDER BY
                level.cts DESC";
    // Get the total count of surveys
    $countQuery = "SELECT COUNT(*) AS total FROM ($query) AS count";
    $countResult = mysqli_query($conn, $countQuery);
    $totalCount = 0;
    if ($countResult) {
        $countRow = mysqli_fetch_assoc($countResult);
        $totalCount = $countRow['total'];
    }

    // Add pagination to the main query
    if ($page !== null && $perPage !== null) {
        $start = ($page - 1) * $perPage;
        $query .= " LIMIT $start, $perPage";
    }
    $query_run = mysqli_query($conn, $query);

    if ($query_run) {
        if (mysqli_num_rows($query_run) > 0) {
            $res = mysqli_fetch_all($query_run, MYSQLI_ASSOC);
            $data = [
                'status' => 201,
                'message' => 'Levels list Fetched Successfully',
                'data' => $res
            ];
                        // Add pagination information to the response
                        if ($page !== null && $perPage !== null) {
                            $data['pagination'] = [
                                'per_page' => $perPage,
                                'total' => $totalCount,
                                'current_page' => $page,
                                'last_page' => ceil($totalCount / $perPage)
                            ];
                        }
            return json_encode($data);
        } else {
            $data = [
                'status' => 404,
                'data' => [],
                'message' => 'No Levels Found'
            ];
            header("HTTP/1.0 404 No Levels Found");
            return json_encode($data);
        }
    } else {
        $data = [
            'status' => 500,
            'message' => 'Internal Server Error',
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }
}

function getAllLevelsList($user_id)
{
    global $conn;

    $user_gender = $user_region_id = $user_profession_id = $user_sub_profession_id = null;

    if ($user_id !== null) {
        // Fetch the user's gender from the user_registration table
        $user_gender_query = "SELECT gender ,region_id,profession_id,sub_profession_id FROM user_registration WHERE user_id = $user_id LIMIT 1";
        $user_gender_result = mysqli_query($conn, $user_gender_query);
        $user_gender_row = mysqli_fetch_assoc($user_gender_result);
        $user_gender = $user_gender_row['gender'];
        $user_region_id = $user_gender_row['region_id'];
        $user_profession_id = $user_gender_row['profession_id'];
        $user_sub_profession_id = $user_gender_row['sub_profession_id'];
    }

    $query = "SELECT
                level.*,
                user_registration.user_name,
                user_registration.full_name,
                COUNT(CASE WHEN (survey_header.status = 1 OR survey_header.status = 0) AND (survey_header.gender = '0' OR survey_header.gender = '$user_gender' ) AND (survey_header.region_id = '0' OR survey_header.region_id = '$user_region_id') AND (survey_header.profession_id = '0' OR survey_header.profession_id = '$user_profession_id') AND (survey_header.sub_profession_id = '0' OR survey_header.sub_profession_id = '$user_sub_profession_id') AND survey_header.user_id <> $user_id THEN survey_header.level_id END) AS surveyCount,
                IFNULL(filled_surveys.filledCount, 0) AS filledCount,
                (COUNT(CASE WHEN (survey_header.status = 1 OR survey_header.status = 0) AND (survey_header.gender = '0' OR survey_header.gender = '$user_gender' ) AND (survey_header.region_id = '0' OR survey_header.region_id = '$user_region_id') AND (survey_header.profession_id = '0' OR survey_header.profession_id = '$user_profession_id') AND (survey_header.sub_profession_id = '0' OR survey_header.sub_profession_id = '$user_sub_profession_id') AND survey_header.user_id <> $user_id THEN survey_header.level_id END) - IFNULL(filled_surveys.filledCount, 0)) AS remainingCount
            FROM
                level
            LEFT JOIN
                survey_header ON survey_header.level_id = level.level_id
            JOIN
                user_registration ON level.user_id = user_registration.user_id
            LEFT JOIN
                (
                    SELECT
                        survey_header.level_id,
                        COUNT(fill_survey_header.survey_id) AS filledCount
                    FROM
                        fill_survey_header
                    JOIN
                        survey_header ON fill_survey_header.survey_id = survey_header.survey_id
                    WHERE
                        fill_survey_header.user_id = " . ($user_id ? $user_id : "NULL") . "
                    GROUP BY
                        survey_header.level_id
                ) AS filled_surveys ON filled_surveys.level_id = level.level_id
            WHERE
                level.del_flag = '1'
            GROUP BY
                level.level_id
            ORDER BY
                level.level_id ASC";
    
    $query_run = mysqli_query($conn, $query);

    if ($query_run) {
        if (mysqli_num_rows($query_run) > 0) {
            $res = mysqli_fetch_all($query_run, MYSQLI_ASSOC);
            $data = [
                'status' => 201,
                'message' => 'Levels list Fetched Successfully',
                'data' => $res
            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 404,
                'data' => [],
                'message' => 'No Levels Found'
            ];
            header("HTTP/1.0 404 No Levels Found");
            return json_encode($data);
        }
    } else {
        $data = [
            'status' => 500,
            'message' => 'Internal Server Error',
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }
}








function updateLevel($inputData, $id)
{
    global $conn;
    $level = mysqli_real_escape_string($conn, trim($inputData['level']));
    $description = mysqli_real_escape_string($conn, $inputData['description']);
    $points_after_fill = mysqli_real_escape_string($conn, $inputData['points_after_fill']);
    $points_after_create = mysqli_real_escape_string($conn, $inputData['points_after_create']);
    $limit_from = mysqli_real_escape_string($conn, $inputData['limit_from']);
    $limit_to = mysqli_real_escape_string($conn, $inputData['limit_to']);
    $user_id = mysqli_real_escape_string($conn, $inputData['user_id']);

    if (empty(trim($level))) {
        return error422('Enter level');
    } elseif (empty(trim($user_id))) {
        return error422('user id is required');
    } elseif ($limit_from=='') {
        return error422('Enter from limit');
    }  elseif ($limit_to=='') {
        return error422('Enter to limit');
    }  elseif (empty(trim($points_after_fill))) {
        return error422('Enter points after fill');
    } elseif (empty(trim($points_after_create))) {
        return error422('Enter points after create');
    } else {
        $levelCheckQuery = "SELECT level_id FROM level WHERE level = '$level' AND level_id <> $id AND  user_id='$user_id' AND del_flag=1";
        $levelCheckResult = mysqli_query($conn, $levelCheckQuery);
        if (mysqli_num_rows($levelCheckResult) > 0) {
            return error422('This level already exists');
        }
        $query = "UPDATE level SET level = '$level', description = '$description',points_after_fill='$points_after_fill',points_after_create='$points_after_create',limit_from='$limit_from',limit_to='$limit_to', user_id = $user_id WHERE level_id = $id";
        $result = mysqli_query($conn, $query);
        if ($result) {
            $data = [
                'status' => 201,
                'message' => 'The level has been updated successfully'
            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 500,
                'message' => 'Internal Server Error'
            ];
            header("HTTP/1.0 500 Internal Server Error");
            return json_encode($data);
        }
    }
}
function deleteLevel($id)
{
    global $conn;
    if (empty(trim($id))) {
        return [
            'status' => 422,
            'message' => 'Id Not Found In URL'
        ];
    } else {
        $query = "UPDATE level SET del_flag = '0' WHERE level_id='$id'";
        $result = mysqli_query($conn, $query);

        if ($result) {
            $data = [
                'status' => 201,
                'message' => 'The Level has been Delete successfully'
            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 500,
                'message' => 'Internal Server Error'
            ];
            header("HTTP/1.0 500 Internal Server Error");
            return json_encode($data);
        }
    }
}
?>