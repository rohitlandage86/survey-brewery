<?php
require '../../../connection/conn.php';
function error422($message)
{
    $data = [
        'status' => 422,
        'message' => $message,
    ];
    header("HTTP/1.0 422 unprocessable Entity");
    echo json_encode($data);
    exit();
}
function createProfession($inputData)
{
    global $conn;
    $profession = mysqli_real_escape_string($conn, trim($inputData['profession']));
    // $sub_profession = mysqli_real_escape_string($conn, $inputData['sub_profession']);
    $description = mysqli_real_escape_string($conn, $inputData['description']);
    $user_id = mysqli_real_escape_string($conn, $inputData['user_id']);
    if (empty(trim($profession))) {
        return error422('Enter profession');
    }
    // elseif (trim($profession) === "student" && empty($sub_profession)) {
    //     return error422('Sub profession is required for student');
    // }
    elseif (empty(trim($user_id))) {
        return error422('user id is required');
    } else {
        // Check if the profession already exists 
        $query = "SELECT * FROM profession WHERE user_id='$user_id' AND profession='$profession' AND del_flag=1";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) > 0) {
            return error422('This profession already exists ');
        }
        // sub_profession, '$sub_profession',
        $query = "INSERT INTO profession (profession,description,user_id) VALUES ('$profession','$description',$user_id)";
        $result = mysqli_query($conn, $query);
        if ($result) {
            $data = [
                'status' => 201,
                'message' => 'The profession has created successfully'

            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 500,
                'message' => 'Internal Server Error',
            ];
            header("HTTP/1.0 500 Internal Server Error");
            return json_encode($data);
        }
    }
}
function getAllProfessionsList($page, $perPage)
{
    global $conn;
    $query = "SELECT profession.*, user_registration.user_name,user_registration.full_name FROM profession LEFT JOIN user_registration ON profession.user_id = user_registration.user_id WHERE  profession.del_flag = '1' ORDER BY `profession`.`cts` DESC";
    // Get the total count of surveys
    $countQuery = "SELECT COUNT(*) AS total FROM ($query) AS count";
    $countResult = mysqli_query($conn, $countQuery);
    $totalCount = 0;
    if ($countResult) {
        $countRow = mysqli_fetch_assoc($countResult);
        $totalCount = $countRow['total'];
    }

    // Add pagination to the main query
    if ($page !== null && $perPage !== null) {
        $start = ($page - 1) * $perPage;
        $query .= " LIMIT $start, $perPage";
    }
    $query_run = mysqli_query($conn, $query);

    if ($query_run) {
        if (mysqli_num_rows($query_run) > 0) {
            $res = mysqli_fetch_all($query_run, MYSQLI_ASSOC);
            $data = [
                'status' => 201,
                'message' => 'Profession list Fetched Successfully',
                'data' => $res
            ];
            // Add pagination information to the response
            if ($page !== null && $perPage !== null) {
                $data['pagination'] = [
                    'per_page' => $perPage,
                    'total' => $totalCount,
                    'current_page' => $page,
                    'last_page' => ceil($totalCount / $perPage)
                ];
            }
            return json_encode($data);
        } else {
            $data = [
                'status' => 404,
                'data' => [],
                'message' => 'No Profession Found'
            ];
            header("HTTP/1.0 404 No Profession Found");
            return json_encode($data);
        }
    } else {
        $data = [
            'status' => 500,
            'message' => 'Internal Server Error',
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }
}
function updateProfession($inputData, $id)
{
    global $conn;
    $profession = mysqli_real_escape_string($conn, trim($inputData['profession']));
    // $sub_profession = mysqli_real_escape_string($conn, $inputData['sub_profession']);
    $description = mysqli_real_escape_string($conn, $inputData['description']);
    $user_id = mysqli_real_escape_string($conn, $inputData['user_id']);

    if (empty(trim($profession))) {
        return error422('Enter profession');
    }
    // elseif (trim($profession) === "student" && empty($sub_profession)) {
    //     return error422('Sub profession is required for student');
    // }  
    elseif (empty(trim($user_id))) {
        return error422('user id is required');
    } else {
        $professionCheckQuery = "SELECT profession_id FROM profession WHERE profession = '$profession' AND profession_id <> $id AND  user_id='$user_id' AND del_flag=1";
        $professionCheckResult = mysqli_query($conn, $professionCheckQuery);
        if (mysqli_num_rows($professionCheckResult) > 0) {
            return error422('This profession already exists');
        }
        // sub_profession = '$sub_profession',
        $query = "UPDATE profession SET profession = '$profession', description = '$description', user_id = $user_id WHERE profession_id = $id";
        $result = mysqli_query($conn, $query);

        if ($result) {
            $data = [
                'status' => 201,
                'message' => 'The profession has been updated successfully'
            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 500,
                'message' => 'Internal Server Error'
            ];
            header("HTTP/1.0 500 Internal Server Error");
            return json_encode($data);
        }
    }
}

function deleteProfession($id)
{
    global $conn;
    if (empty(trim($id))) {
        return [
            'status' => 422,
            'message' => 'Id Not Found In URL'
        ];
    } else {
        $query = "UPDATE profession SET del_flag = '0' WHERE profession_id='$id'";
        $result = mysqli_query($conn, $query);

        if ($result) {
            $data = [
                'status' => 201,
                'message' => 'The profession has been deleted successfully'
            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 500,
                'message' => 'Internal Server Error'
            ];
            header("HTTP/1.0 500 Internal Server Error");
            return json_encode($data);
        }
    }
}
