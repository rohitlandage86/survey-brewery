<?php
require '../../connection/conn.php';
function error422($message)
{
    $data = [
        "status" => 422,
        "message" => $message
    ];
    header("HTTP/1.0 422 unprocessable Entity");
    echo json_encode($data);
    exit();
}

function getAllAdminCount1()
{
    global $conn;
    // Get users
    $users = 0;
    $del_flag = 1; // Create a variable to store the value 1
    $query = "SELECT COUNT(*) FROM user_registration ur JOIN untitled u on u.user_id=ur.user_id   WHERE ur.del_flag=? AND u.category=2";
    $stmt = mysqli_prepare($conn, $query);
    mysqli_stmt_bind_param($stmt, "i", $del_flag);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $users);
    mysqli_stmt_fetch($stmt);
    mysqli_stmt_close($stmt);

    // Get all live survey count
    $liveSurveyCount = 0;
    $status = 1; // Create a variable to store the value 1
    $query = "SELECT COUNT(*) FROM survey_header  WHERE status=?";
    $stmt = mysqli_prepare($conn, $query);
    mysqli_stmt_bind_param($stmt, "i", $status);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $liveSurveyCount);
    mysqli_stmt_fetch($stmt);
    mysqli_stmt_close($stmt);

    // Get all closed survey count
    $closedSurveyCount = 0;
    $status = 0; // Create a variable to store the value 1
    $query = "SELECT COUNT(*) FROM survey_header  WHERE status=?";
    $stmt = mysqli_prepare($conn, $query);
    mysqli_stmt_bind_param($stmt, "i", $status);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $closedSurveyCount);
    mysqli_stmt_fetch($stmt);
    mysqli_stmt_close($stmt);

    // Get the live survey count by date
    $live_survey_date = [];
    $maxQuery = "SELECT min(start_date) FROM survey_header WHERE status = 1";
    $maxResult = mysqli_query($conn, $maxQuery);
    $maxDate = mysqli_fetch_assoc($maxResult);
    // print_r($maxDate);




    $data = [
        'status' => 201,
        'message' => 'Admin Count Fetched Successfully',
        'data' => [
            'users' => $users,
            'liveSurveyCount' => $liveSurveyCount,
            'closedSurveyCount' => $closedSurveyCount,
            'totalSurveyCount' => $liveSurveyCount + $closedSurveyCount,
            'live_survey_date' => $live_survey_date
        ]
    ];
    return json_encode($data);
}

function getAllAdminCount()
{
    global $conn;
    // Get users
    $users = 0;
    $del_flag = 1; // Create a variable to store the value 1
    $query = "SELECT COUNT(*) FROM user_registration ur JOIN untitled u on u.user_id=ur.user_id   WHERE ur.del_flag=? AND u.category=2";
    $stmt = mysqli_prepare($conn, $query);
    mysqli_stmt_bind_param($stmt, "i", $del_flag);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $users);
    mysqli_stmt_fetch($stmt);
    mysqli_stmt_close($stmt);

    // Get all live survey count
    $liveSurveyCount = 0;
    $status = 1; // Create a variable to store the value 1
    $query = "SELECT COUNT(*) FROM survey_header  WHERE status=?";
    $stmt = mysqli_prepare($conn, $query);
    mysqli_stmt_bind_param($stmt, "i", $status);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $liveSurveyCount);
    mysqli_stmt_fetch($stmt);
    mysqli_stmt_close($stmt);

    // Get all closed survey count
    $closedSurveyCount = 0;
    $status = 0; // Create a variable to store the value 1
    $query = "SELECT COUNT(*) FROM survey_header  WHERE status=?";
    $stmt = mysqli_prepare($conn, $query);
    mysqli_stmt_bind_param($stmt, "i", $status);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $closedSurveyCount);
    mysqli_stmt_fetch($stmt);
    mysqli_stmt_close($stmt);

    // Get the live survey count by date
    $live_survey_date = [];
    $maxQuery = "SELECT min(start_date) FROM survey_header WHERE status = 1";
    $maxResult = mysqli_query($conn, $maxQuery);
    $maxDate = mysqli_fetch_assoc($maxResult);
    // Get the current month's start and end dates
    $currentYear = date('Y');
    $currentMonth = date('m');
    $firstDayOfMonth = date('Y-m-d', strtotime("first day of $currentYear-$currentMonth"));
    $lastDayOfMonth = date('Y-m-d', strtotime("last day of $currentYear-$currentMonth"));
    // Get the live survey count by date for the current month
    $live_survey_date = [];
    $query = "SELECT DATE(start_date) AS surveyDate, COUNT(*) AS surveyCount FROM survey_header WHERE status = 1 AND start_date >= ? AND start_date <= ? GROUP BY surveyDate";
    $stmt = mysqli_prepare($conn, $query);
    mysqli_stmt_bind_param($stmt, "ss", $firstDayOfMonth, $lastDayOfMonth);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);

    while ($row = mysqli_fetch_assoc($result)) {
        $date = date('d-m-Y', strtotime($row['surveyDate']));
        $surveyCount = $row['surveyCount'];
        $live_survey_date[] = ['date' => $date, 'surveyCount' => $surveyCount];
    }
    mysqli_free_result($result);
    mysqli_stmt_close($stmt);

    // Add zero survey counts for dates with no surveys in the current month
    $currentDate = strtotime($firstDayOfMonth);
    $lastDate = strtotime($lastDayOfMonth);

    while ($currentDate <= $lastDate) {
        $formattedDate = date('d-m-Y', $currentDate);
        $surveyCount = isset($live_survey_date[$formattedDate]) ? $live_survey_date[$formattedDate]['surveyCount'] : 0;
        $live_survey_date[] = ['date' => $formattedDate, 'surveyCount' => $surveyCount];
        $currentDate = strtotime('+1 day', $currentDate);
    }
    usort($live_survey_date, function ($a, $b) {
        return strtotime($a['date']) - strtotime($b['date']);
    });

    $data = [
        'status' => 201,
        'message' => 'Admin Count Fetched Successfully',
        'data' => [
            'users' => $users,
            'liveSurveyCount' => $liveSurveyCount,
            'closedSurveyCount' => $closedSurveyCount,
            'totalSurveyCount' => $liveSurveyCount + $closedSurveyCount,
            'live_survey_date' => $live_survey_date
        ]
    ];
    return json_encode($data);
}
