<?php
// error_reporting(0);
error_reporting(E_ALL);
ini_set('display_errors', 1);
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include('function.php');
$requestMethod = $_SERVER["REQUEST_METHOD"];
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    http_response_code(200);
    exit;
}

if ($requestMethod=="POST") {
    $data = json_decode(file_get_contents("php://input"),true);
   if (empty($data)) {
    $loginUser=loginUser($_POST);
   } else {
    $loginUser= loginUser($data);
   }
   echo $loginUser;
}else{
    $data=[
        'status'=>405,
        'message'=> $requestMethod.' Method Not Allowed'
    ];
    header("HTTP/1.0 405 Method Not Allowed");
    echo json_encode($data);
}
?>