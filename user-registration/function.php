<?php
require '../connection/conn.php';
function error422($message)
{
    $data = [
        'status' => 422,
        'message' => $message,
    ];
    header("HTTP/1.0 422 unprocessable Entity");
    echo json_encode($data);
    exit();
}

function createUser($inputData)
{
    global $conn;
    $user_name = mysqli_real_escape_string($conn, $inputData['user_name']);
    $full_name = mysqli_real_escape_string($conn, $inputData['full_name']);
    $gender = mysqli_real_escape_string($conn, $inputData['gender']);
    $region_id = mysqli_real_escape_string($conn, $inputData['region_id']);
    $profession_id = mysqli_real_escape_string($conn, $inputData['profession_id']);
    $sub_profession_id = mysqli_real_escape_string($conn, $inputData['sub_profession_id']);
    $email_id = mysqli_real_escape_string($conn, $inputData['email_id']);
    $extenstions = mysqli_real_escape_string($conn, $inputData['extenstions']);

    if (empty(trim($user_name))) {
        return error422('Enter username');
    } elseif (empty(trim($full_name))) {
        return error422('Enter full name');
    } elseif (empty(trim($gender))) {
        return error422('Select gender');
    } elseif (empty(trim($region_id))) {
        return error422('Select region');
    } elseif (empty(trim($email_id))) {
        return error422('Enter email');
    } elseif (trim($profession_id) === "1" && empty($sub_profession_id)) {
        return error422('Sub profession is required for students');
    } elseif (empty(trim($extenstions))) {
        return error422('Please set a password');
    } else {
        // Check if the email id already exists in the untitled table
        $query = "SELECT * FROM untitled WHERE email_id='$email_id' AND del_flag=1";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) > 0) {
            return error422('This email id already exists');
        }
        // Check if the user name already exists in the user_registration table
        $query = "SELECT * FROM user_registration WHERE user_name='$user_name' AND del_flag=1";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) > 0) {
            return error422('This user name already exists');
        }
        // Insert user data into user_registration table
        $query = "INSERT INTO user_registration (user_name, full_name, gender, profession_id, sub_profession_id, region_id, latitude, longitude, ip) 
                  VALUES ('$user_name', '$full_name', $gender, '$profession_id', '$sub_profession_id', $region_id,NULL,NULL, NULL)";
        $result1 = mysqli_query($conn, $query);
        $user_id = mysqli_insert_id($conn); // Retrieve user_id from user_registration table

        // Generate a token and insert it into the jeton table
        $length = 200; // Desired string length
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $token = '';
        for ($i = 0; $i < $length; $i++) {
            $token .= $characters[rand(0, strlen($characters) - 1)];
        }
        $currentDateTime = date('Y-m-d H:i:s');
        $create_token = "INSERT INTO jeton (jeton, utilisatrice, expiresin) VALUES ('$token', $user_id, '$currentDateTime')";
        $jeton_result = mysqli_query($conn, $create_token);

        // Hash the password and insert it into the untitled table
        $password = hash('sha256', $extenstions);
        $create_untitled = "INSERT INTO untitled (user_id, email_id, extenstions) VALUES ($user_id, '$email_id', '$password')";
        $result2 = mysqli_query($conn, $create_untitled);
        $getUserDetails = "SELECT * FROM user_registration WHERE user_id ='$user_id'";
        $getUserDetailsResults = mysqli_query($conn, $getUserDetails);
        $userData = mysqli_fetch_assoc($getUserDetailsResults);

        if ($result1 && $result2) {
            $data = [
                'status' => 201,
                'message' => 'The user has registered successfully',
                'user_id' => $user_id,
                'user_name' => $user_name,
                'userData' => $userData,
                'token' => $token,
                'expiresin' => $currentDateTime,
            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 500,
                'message' => 'Internal Server Error',
            ];
            header("HTTP/1.0 500 Internal Server Error");
            return json_encode($data);
        }
    }
}


function getAllUserRegisterList($page,$perPage)
{
    global $conn;
    $query = "SELECT u.*, t.email_id ,r.region FROM user_registration u JOIN untitled t ON u.user_id = t.user_id JOIN regions r ON u.region_id = r.region_id WHERE u.del_flag = '1' AND t.category = '2' ORDER BY `u`.`cts` DESC";
        // Get the total count of surveys
        $countQuery = "SELECT COUNT(*) AS total FROM ($query) AS count";
        $countResult = mysqli_query($conn, $countQuery);
        $totalCount = 0;
        if ($countResult) {
            $countRow = mysqli_fetch_assoc($countResult);
            $totalCount = $countRow['total'];
        }
                // // Add pagination to the main query
                if ($page !== null && $perPage !== null) {
                    $start = ($page - 1) * $perPage;
                    $query .= " LIMIT $start, $perPage";
                }
    $query_run = mysqli_query($conn, $query);

    

    if ($query_run) {
        if (mysqli_num_rows($query_run) > 0) {
            $res = mysqli_fetch_all($query_run, MYSQLI_ASSOC);
            $data = [
                'status' => 201,
                'message' => 'Users list Fetched Successfully',
                'data' => $res
            ];
                        // Add pagination information to the response
                        if ($page !== null && $perPage !== null) {
                            $data['pagination'] = [
                                'per_page' => $perPage,
                                'total' => $totalCount,
                                'current_page' => $page,
                                'last_page' => ceil($totalCount / $perPage)
                            ];
                        }
            return json_encode($data);

        } else {
            $data = [
                'status' => 404,
                'data'=>[],
                'message' => 'No Users Found'
            ];
            header("HTTP/1.0 404 No Users Found");
            return json_encode($data);
        }

    } else {
        $data = [
            'status' => 500,
            'message' => 'Internal Server Error',
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }



}
function loginUser($loginInput){
    global $conn;
    $email_id = mysqli_real_escape_string($conn, $loginInput['email_id']);
    $extenstions = mysqli_real_escape_string($conn, $loginInput['password']);
  
    if (empty(trim($email_id))) {
        return error422('Enter email id');
    } elseif (empty(trim($extenstions))) {
        return error422('Enter password');
    } else {
        $password = hash('sha256', $extenstions);
        $query = "SELECT * FROM untitled WHERE email_id='$email_id' AND extenstions='$password' AND del_flag=1";
        $result = mysqli_query($conn, $query);
        $user = mysqli_fetch_assoc($result);
        if (!$user) {
            $data = [
                'status' => 401,
                'message' => 'Invalid email or user not found',
            ];
            header("HTTP/1.0 401 Unauthorized");
            return json_encode($data);
        }

        $user_id=$user['user_id'];
        $length = 200; // Desired string length
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $token = '';
        
        for ($i = 0; $i < $length; $i++) {
            $token .= $characters[rand(0, strlen($characters) - 1)];
        }
        $currentDateTime = date('Y-m-d H:i:s');
        $create_token="UPDATE jeton SET jeton='$token', expiresin='$currentDateTime'  WHERE  utilisatrice='$user_id'";
        $jeton_result = mysqli_query($conn, $create_token); 

        $query2 = "SELECT * FROM user_registration WHERE user_id ='$user_id'";
        $result2 = mysqli_query($conn, $query2);
        $userData = mysqli_fetch_assoc($result2);
        if ($user['del_flag']=="1") {
            if ($user['category']=="1") {
                $data = [
                    'status' => 200,
                    'message' => 'Admin logged in successfully',
                    'data' => [
                        'userData' =>  $userData,
                        'email_id' => $user['email_id'],
                        'category' => $user['category'],
                        'token' => $token,
                        'expiresin' => $currentDateTime,
                    ]
                ];
                return json_encode($data);
            } else {
                $data = [
                    'status' => 200,
                    'message' => 'User logged in successfully',
                    'data' => [
                        'userData' =>  $userData,
                        'email_id' => $user['email_id'],
                        'category' => $user['category'],
                        'token' => $token,
                        'expiresin' => $currentDateTime,
                    ]
                ];
                return json_encode($data);
            }
            
        } else {
            $data = [
                'status' => 401,
                'message' => 'Invalid username or password',
            ];
            header("HTTP/1.0 401 Unauthorized");
            return json_encode($data);
        }
    }
}
function loginWithGoogle($inputData)
{
    global $conn;
    $email_id = mysqli_real_escape_string($conn, $inputData['email_id']);

    if (empty(trim($email_id))) {
        return error422('Access denied');
    } else {
        $query = "SELECT * FROM untitled WHERE email_id='$email_id' AND del_flag=1";
        $result = mysqli_query($conn, $query);
        $user = mysqli_fetch_assoc($result);

        if (!$user) {
            $data = [
                'status' => 401,
                'message' => 'Invalid email or user not found',
            ];
            header("HTTP/1.0 401 Unauthorized");
            return json_encode($data);
        }

        $user_id = $user['user_id'];
        $query2 = "SELECT * FROM user_registration WHERE user_id ='$user_id'";
        $result2 = mysqli_query($conn, $query2);
        $userData = mysqli_fetch_assoc($result2);

        $length = 200; // Desired string length
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $token = '';
        
        for ($i = 0; $i < $length; $i++) {
            $token .= $characters[rand(0, strlen($characters) - 1)];
        }
        $currentDateTime = date('Y-m-d H:i:s');
        $create_token = "UPDATE jeton SET jeton='$token', expiresin='$currentDateTime'  WHERE  utilisatrice='$user_id'";
        $jeton_result = mysqli_query($conn, $create_token); 

        if ($user['del_flag'] == "1") {
            $category = isset($user['category']) ? $user['category'] : null;

            if ($category === "1") {
                $data = [
                    'status' => 200,
                    'message' => 'Admin logged in successfully',
                    'data' => [
                        'userData' =>  $userData,
                        'email_id' => $user['email_id'],
                        'category' => $category,
                        'token' => $token,
                        'expiresin' => $currentDateTime,
                    ]
                ];
                return json_encode($data);
            } else {
                $data = [
                    'status' => 200,
                    'message' => 'User logged in successfully',
                    'data' => [
                        'userData' =>  $userData,
                        'email_id' => $user['email_id'],
                        'category' => $category,
                        'token' => $token,
                        'expiresin' => $currentDateTime,
                    ]
                ];
                return json_encode($data);
            }
        } else {
            $data = [
                'status' => 401,
                'message' => 'Invalid username or password',
            ];
            header("HTTP/1.0 401 Unauthorized");
            return json_encode($data);
        }
    }
}


function userDetailsById($user_id){
    global $conn;
    $query = "SELECT user_registration.*, sub_profession.sub_profession, profession.profession, regions.region,untitled.email_id  
    FROM user_registration 
    LEFT JOIN sub_profession ON user_registration.sub_profession_id= sub_profession.sub_profession_id    
    JOIN profession ON user_registration.profession_id = profession.profession_id   
    JOIN regions ON user_registration.region_id = regions.region_id 
    JOIN untitled  ON user_registration.user_id = untitled.user_id   
    WHERE  user_registration.user_id = $user_id AND user_registration.del_flag = '1';";
    $query_run = mysqli_query($conn, $query);
    // print_r($query);
    if ($query_run) {
        
        if (mysqli_num_rows($query_run) > 0) {
            $res = mysqli_fetch_all($query_run, MYSQLI_ASSOC);
            $data = [
                'status' => 201,
                'message' => 'user Details Fetched Successfully',
                'data' => $res
            ];
            return json_encode($data);

        } else {
            $data = [
                'status' => 404,
                'data'=>[],
                'message' => 'No user Details Found'
            ];
            header("HTTP/1.0 404 No user Details Found");
            return json_encode($data);
        }

    } else {
        $data = [
            'status' => 500,
            'message' => 'Internal Server Error',
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }

}
function updateUser($inputData, $id)
{
    global $conn;
    $user_name = mysqli_real_escape_string($conn, $inputData['user_name']);
    $full_name = mysqli_real_escape_string($conn, $inputData['full_name']);
    $gender = mysqli_real_escape_string($conn, $inputData['gender']);
    $region_id = mysqli_real_escape_string($conn, $inputData['region_id']);
    $profession_id = mysqli_real_escape_string($conn, $inputData['profession_id']);
    $sub_profession_id = mysqli_real_escape_string($conn, $inputData['sub_profession_id']);
    $email_id = mysqli_real_escape_string($conn, $inputData['email_id']);

    if (empty(trim($user_name))) {
        return error422('Enter username');
    } elseif (empty(trim($full_name))) {
        return error422('Enter full name');
    } elseif (empty(trim($gender))) {
        return error422('Select gender');
    } elseif (empty(trim($region_id))) {
        return error422('Select region');
    } elseif (empty(trim($email_id))) {
        return error422('Enter email');
    } elseif (isset($inputData['profession_id']) && trim($inputData['profession_id']) === "1" && (!isset($inputData['sub_profession_id']) || empty($inputData['sub_profession_id']))) {
        return error422('Sub profession is required for students');
    } else {
        // Check if the email id already exists in the untitled table
        $query = "SELECT * FROM untitled WHERE email_id='$email_id' AND del_flag=1 AND user_id <> $id";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) > 0) {
            return error422('This email id already exists');
        }

        // Check if the user name already exists in the user_registration table, excluding the current user's ID
        $query = "SELECT * FROM user_registration WHERE user_name='$user_name' AND user_id <> $id AND del_flag=1";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) > 0) {
            return error422('This user name already exists');
        }

      

        $query = "UPDATE user_registration SET user_name='$user_name', full_name='$full_name', gender='$gender', region_id='$region_id', profession_id='$profession_id', sub_profession_id='$sub_profession_id' WHERE user_id=$id";
        $result1 = mysqli_query($conn, $query);

        $update_untitled = "UPDATE untitled SET email_id='$email_id' WHERE user_id=$id";
        $result2 = mysqli_query($conn, $update_untitled);

        $getUserDetails = "SELECT * FROM user_registration WHERE user_id =$id";
        $getUserDetailsResults = mysqli_query($conn, $getUserDetails);
        $userData = mysqli_fetch_assoc($getUserDetailsResults);

        if ($result1 && $result2) {
            $data = [
                'status' => 200,
                'message' => 'The user has been updated successfully',
                'user_id' => $id,
                'user_name' => $user_name,
                'userData' => $userData
            ];
            return json_encode($data);
        } else {
            $data = [
                'status' => 500,
                'message' => 'Internal Server Error',
            ];
            header("HTTP/1.0 500 Internal Server Error");
            return json_encode($data);
        }
    }
}

function getKarmaPointHistory($page,$perPage,$user_id){
    global $conn;
    $query = "SELECT kp.* , sh.survey_title FROM karma_points_log kp  JOIN survey_header sh ON sh.survey_id=kp.survey_id WHERE kp.user_id = $user_id";
    $query .= " ORDER BY kp.cts desc";

    // Get the total count of karma point history list
    $countQuery = "SELECT COUNT(*) AS total FROM ($query) AS count";
    $countResult = mysqli_query($conn, $countQuery);
    $totalCount = 0;
    if ($countResult) {
        $countRow = mysqli_fetch_assoc($countResult);
        $totalCount = $countRow['total'];
    }


    // Add pagination to the main query
    if ($page !== null && $perPage !== null) {
        $start = ($page - 1) * $perPage;
        $query .= " LIMIT $start, $perPage";
    }
    
    $query_run = mysqli_query($conn, $query);
    if ($query_run) {
        if (mysqli_num_rows($query_run) > 0) {
            $res = mysqli_fetch_all($query_run, MYSQLI_ASSOC);
            $data = [
                'status' => 201,
                'message' => 'karma point history list',
                'data' => $res
            ];

            // Add pagination information to the response
            if ($page !== null && $perPage !== null) {
                $data['pagination'] = [
                    'per_page' => $perPage,
                    'total' => $totalCount,
                    'current_page' => $page,
                    'last_page' => ceil($totalCount / $perPage)
                ];
            }

            return json_encode($data);
        } else {
            $data = [
                'status' => 404,
                'data' => [],
                'message' => 'No karma point history found'
            ];
            header("HTTP/1.0 404 Not Found");
            return json_encode($data);
        }
    } else {
        $data = [
            'status' => 500,
            'message' => 'Internal Server Error',
        ];
        header("HTTP/1.0 500 Internal Server Error");
        return json_encode($data);
    }
}
?>
